-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Mar-2018 às 02:53
-- Versão do servidor: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `corretora`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `funcao` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `usuario` varchar(20) DEFAULT NULL,
  `senha` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluguel`
--

CREATE TABLE `aluguel` (
  `id` int(11) NOT NULL,
  `idImovel` int(11) DEFAULT NULL,
  `idLocador` int(11) DEFAULT NULL,
  `jurosPercentual` float(9,2) DEFAULT '0.00',
  `multaPercentual` float(9,2) DEFAULT '0.00',
  `dataInicio` date DEFAULT NULL,
  `dataFim` date DEFAULT NULL,
  `vencimentoAluguel` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bairro`
--

CREATE TABLE `bairro` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `idCidade` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE `cidade` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `idEstado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `id` int(11) NOT NULL,
  `logradouro` varchar(50) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `complemento` varchar(80) DEFAULT NULL,
  `idBairro` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `uf` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `imovel`
--

CREATE TABLE `imovel` (
  `id` int(11) NOT NULL,
  `idLocatario` int(11) DEFAULT NULL,
  `idEndereco` int(11) DEFAULT NULL,
  `area` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `tamanho` int(11) DEFAULT NULL,
  `valorCondominio` float(9,2) DEFAULT '0.00',
  `valorIptu` float(9,2) DEFAULT '0.00',
  `valorAluguel` float(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `locador`
--

CREATE TABLE `locador` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `cpf` varchar(30) DEFAULT NULL,
  `rg` varchar(30) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagamento`
--

CREATE TABLE `pagamento` (
  `id` int(11) NOT NULL,
  `idAluguel` int(11) DEFAULT NULL,
  `mesRef` date DEFAULT NULL,
  `valor` float(9,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `locatario`
--

CREATE TABLE `locatario` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `conta` varchar(30) DEFAULT NULL,
  `idEndereco` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefone_locador`
--

CREATE TABLE `telefone_locador` (
  `id` int(11) NOT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `idLocador` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefone_locatario`
--

CREATE TABLE `telefone_locatario` (
  `id` int(11) NOT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `idLocatario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `visitacao`
--

CREATE TABLE `visitacao` (
  `id` int(11) NOT NULL,
  `idLocador` int(11) DEFAULT NULL,
  `idImovel` int(11) DEFAULT NULL,
  `dataEntregaChave` date DEFAULT NULL,
  `dataDevolucaoChave` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aluguel`
--
ALTER TABLE `aluguel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aluguel_imovel` (`idImovel`),
  ADD KEY `fk_aluguel_locador` (`idLocador`);

--
-- Indexes for table `bairro`
--
ALTER TABLE `bairro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bairro_cidade` (`idCidade`);

--
-- Indexes for table `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cidade_estado` (`idEstado`);

--
-- Indexes for table `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_endereco_bairro` (`idBairro`);

--
-- Indexes for table `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imovel`
--
ALTER TABLE `imovel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imovel_locatario` (`idLocatario`),
  ADD KEY `fk_imovel_endereco` (`idEndereco`);

--
-- Indexes for table `locador`
--
ALTER TABLE `locador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagamento`
--
ALTER TABLE `pagamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pagamento_aluguel` (`idAluguel`);

--
-- Indexes for table `locatario`
--
ALTER TABLE `locatario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_locatario_endereco` (`idEndereco`);

--
-- Indexes for table `telefone_locador`
--
ALTER TABLE `telefone_locador`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_telefone_locador` (`idLocador`);

--
-- Indexes for table `telefone_locatario`
--
ALTER TABLE `telefone_locatario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_telefone_locatario` (`idLocatario`);

--
-- Indexes for table `visitacao`
--
ALTER TABLE `visitacao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_visitacao_locador` (`idLocador`),
  ADD KEY `fk_visitacao_imovel` (`idImovel`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aluguel`
--
ALTER TABLE `aluguel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bairro`
--
ALTER TABLE `bairro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cidade`
--
ALTER TABLE `cidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `endereco`
--
ALTER TABLE `endereco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `imovel`
--
ALTER TABLE `imovel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locador`
--
ALTER TABLE `locador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pagamento`
--
ALTER TABLE `pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locatario`
--
ALTER TABLE `locatario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `telefone_locador`
--
ALTER TABLE `telefone_locador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `telefone_locatario`
--
ALTER TABLE `telefone_locatario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visitacao`
--
ALTER TABLE `visitacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `aluguel`
--
ALTER TABLE `aluguel`
  ADD CONSTRAINT `fk_aluguel_imovel` FOREIGN KEY (`idImovel`) REFERENCES `imovel` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aluguel_locador` FOREIGN KEY (`idLocador`) REFERENCES `locador` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `bairro`
--
ALTER TABLE `bairro`
  ADD CONSTRAINT `fk_bairro_cidade` FOREIGN KEY (`idCidade`) REFERENCES `cidade` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `cidade`
--
ALTER TABLE `cidade`
  ADD CONSTRAINT `fk_cidade_estado` FOREIGN KEY (`idEstado`) REFERENCES `estado` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `endereco`
--
ALTER TABLE `endereco`
  ADD CONSTRAINT `fk_endereco_bairro` FOREIGN KEY (`idBairro`) REFERENCES `bairro` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `imovel`
--
ALTER TABLE `imovel`
  ADD CONSTRAINT `fk_imovel_endereco` FOREIGN KEY (`idEndereco`) REFERENCES `endereco` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_imovel_locatario` FOREIGN KEY (`idLocatario`) REFERENCES `locatario` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `pagamento`
--
ALTER TABLE `pagamento`
  ADD CONSTRAINT `fk_pagamento_aluguel` FOREIGN KEY (`idAluguel`) REFERENCES `aluguel` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `locatario`
--
ALTER TABLE `locatario`
  ADD CONSTRAINT `fk_locatario_endereco` FOREIGN KEY (`idEndereco`) REFERENCES `endereco` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `telefone_locador`
--
ALTER TABLE `telefone_locador`
  ADD CONSTRAINT `fk_telefone_locador` FOREIGN KEY (`idLocador`) REFERENCES `locador` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `telefone_locatario`
--
ALTER TABLE `telefone_locatario`
  ADD CONSTRAINT `fk_telefone_locatario` FOREIGN KEY (`idLocatario`) REFERENCES `locatario` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `visitacao`
--
ALTER TABLE `visitacao`
  ADD CONSTRAINT `fk_visitacao_imovel` FOREIGN KEY (`idImovel`) REFERENCES `imovel` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_visitacao_locador` FOREIGN KEY (`idLocador`) REFERENCES `locador` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
