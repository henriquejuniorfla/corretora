<?php
	require_once("util/autoload.php");
	
	$locatarioController = new LocatarioController();
	$locatarios = $locatarioController->obterTodos();

	$imovel = null;
	
	if(isset($_GET['id']) && is_numeric($_GET['id'])){
		$idImovel = $_GET['id'];
		$imovelController = new ImovelController();
		$imovel = $imovelController->obterComId($idImovel);
	}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>HAM Corretora - Cadastrar Imóvel</title>
	<link rel="shortcut icon" href="img/logo.ico" type="image/x-icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="css/owl.theme.default.css" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="css/magnific-popup.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />
	<link type="text/css" rel="stylesheet" href="css/styleCadastro.css" />

</head>

<body>
	<!-- Header -->
	<header>

		<!-- Nav -->
		<nav id="nav" class="navbar">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="index.html">
							<img class="logo" src="img/logo.png" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.html#home">Home</a></li>
					<li><a href="index.html#clientes">Clientes</a></li>
					<li><a href="index.html#imoveis">Imóveis</a></li>
					<li><a href="index.html#controle">Controle</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-9">
					<div class="blog">
						<!-- form -->
						<div class="reply-form">
							<h3 class="title">Cadastrar Imóvel</h3>

							<form id="formulario" data-id="<?php if(isset($imovel) && $imovel->getId() != null) echo $imovel->getId(); ?>">

								<div class="widget">
									<h4>Proprietário</h4>
									<?php 
										if($locatarios != null){
											echo "<select class='selecao' id='selecao-locatario'>";
												echo "<option value=''>Selecione</option>";
											foreach($locatarios as $locatario){ ?>
												<option value="<?php echo $locatario->getId();?>"><?php echo $locatario->getNome(); ?></option>
												
											<?php }
											echo "</select>";
										} else {
											echo "<p> Nenhum proprietário cadastrado! </p>";
										}
									?>
								</div>

								<div class="widget col-md-6">
									<h4>Tamanho</h4>
									<div class="widget-category ">
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::CONJUGADO;?>" checked> <?php echo Tamanho::toString(Tamanho::CONJUGADO); ?></span>
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::UM_QUARTO;?>"> <?php echo Tamanho::toString(Tamanho::UM_QUARTO); ?></span>
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::DOIS_QUARTO;?>"> <?php echo Tamanho::toString(Tamanho::DOIS_QUARTO); ?></span>
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::TRES_QUARTO;?>"> <?php echo Tamanho::toString(Tamanho::TRES_QUARTO); ?></span>
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::QUATRO_OU_MAIS_QUARTOS;?>"> <?php echo Tamanho::toString(Tamanho::QUATRO_OU_MAIS_QUARTOS); ?></span>
									</div>
								</div>

								<div class="widget col-md-6">
									<h4>Área</h4>
									<div class="widget-category ">
										<span><input type="radio" name="area" value="<?php echo Area::CENTRO;?>" checked> <?php echo Area::toString(Area::CENTRO); ?></span>
										<span><input type="radio" name="area" value="<?php echo Area::ZONA_NORTE;?>"> <?php echo Area::toString(Area::ZONA_NORTE); ?></span>
										<span><input type="radio" name="area" value="<?php echo Area::ZONA_SUL;?>"> <?php echo Area::toString(Area::ZONA_SUL); ?></span>
										<span><input type="radio" name="area" value="<?php echo Area::ZONA_OESTE;?>"> <?php echo Area::toString(Area::ZONA_OESTE); ?></span>
									</div>
								</div>

								<div id="tipo" class="widget col-md-6">
									<h4>Tipo</h4>
									<div class="widget-category ">
										<span><input type="radio" name="tipo" value="<?php echo Tipo::CASA; ?>" checked> <?php echo Tipo::toString(Tipo::CASA); ?></span>
										<span><input type="radio" name="tipo" value="<?php echo Tipo::APARTAMENTO; ?>"> <?php echo Tipo::toString(Tipo::APARTAMENTO); ?></span>
									</div>
								</div><br>
		
								<div class="endereco col-md-12">
									<h4>Endereço</h4><br>
									<input id="cep" class="input" type="text" placeholder="CEP" value="<?php if(isset($imovel) && $imovel->getEndereco() != null && $imovel->getEndereco()->getCep() != null) echo $imovel->getEndereco()->getCep(); ?>" required><br>
								<input id="logradouro" class="input" type="text" placeholder="Logradouro" value="<?php if(isset($imovel) && $imovel->getEndereco() != null && $imovel->getEndereco()->getLogradouro() != null) echo $imovel->getEndereco()->getLogradouro(); ?>" required>
								<input id="bairro" class="input" type="text" placeholder="Bairro" value="<?php if(isset($imovel) && $imovel->getEndereco() != null && $imovel->getEndereco()->getBairro() != null && $imovel->getEndereco()->getBairro()->getBairro() != null) echo $imovel->getEndereco()->getBairro()->getBairro(); ?>" required>
								<input id="cidade" class="input" type="text" placeholder="Cidade" value="<?php if(isset($imovel) && $imovel->getEndereco() != null && $imovel->getEndereco()->getBairro()->getCidade() != null && $imovel->getEndereco()->getBairro()->getCidade()->getCidade() != null) echo $imovel->getEndereco()->getBairro()->getCidade()->getCidade(); ?>" required>
								<input id="estado" class="input" type="text" placeholder="Estado" value="<?php if(isset($imovel) && $imovel->getEndereco() != null && $imovel->getEndereco()->getBairro()->getCidade()->getEstado() != null && $imovel->getEndereco()->getBairro()->getCidade()->getEstado()->getEstado() != null) echo $imovel->getEndereco()->getBairro()->getCidade()->getEstado()->getEstado(); ?>" required>
								<input id="uf" class="input" type="text" placeholder="UF" value="<?php if(isset($imovel) && $imovel->getEndereco() != null && $imovel->getEndereco()->getBairro()->getCidade()->getEstado() != null && $imovel->getEndereco()->getBairro()->getCidade()->getEstado()->getUf() != null) echo $imovel->getEndereco()->getBairro()->getCidade()->getEstado()->getUf(); ?>" required>
								<input id="numero" class="input" type="number" placeholder="Número" min="0" value="<?php if(isset($imovel) && $imovel->getEndereco() != null && $imovel->getEndereco()->getNumero() != null) echo $imovel->getEndereco()->getNumero(); ?>">
								<input id="complemento" class="input" type="text" placeholder="Complemento" value="<?php if(isset($imovel) && $imovel->getEndereco() != null && $imovel->getEndereco()->getComplemento() != null) echo $imovel->getEndereco()->getComplemento(); ?>">
								</div>

								<div class="col-md-12">
									<h4>Valor</h4><br>
									<input id="aluguel" class="input" type="text" placeholder="Aluguel">
									<input id="condominio" class="input" type="text" placeholder="Condomínio">
									<input id="iptu" class="input" type="text" placeholder="IPTU">
								</div>

								<div class="col-md-12">
									<button type="button" class="main-btn cadastrar">Cadastrar</button>
								</div>
							</form>
						</div>
						<!-- /form -->
					</div>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->

	<!-- Footer -->
	<footer id="footer" class="sm-padding bg-dark">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<div class="col-md-12">

					<!-- footer logo -->
					<div class="footer-logo">
						<a href="index.html"><img src="img/logo-alt.png" alt="logo"></a>
					</div>
					<!-- /footer logo -->

					<!-- footer copyright -->
					<div class="footer-copyright">
						<p>Copyright © 2017. All Rights Reserved. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
					</div>
					<!-- /footer copyright -->

				</div>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular.js"></script>
	<script>
	var app = angular.module("App", []);
	app.controller("AppController", function($scope) {
		$scope.rows = [];

		$scope.add = function() {
			$scope.rows.push({tel:""});
		};
            
		$scope.remove = function(row) {
			var index = $scope.rows.indexOf(row);
			$scope.rows.splice(index, 1);
		};
	});
	</script>
</body>
</html>

<script type="text/javascript" src="js/endereco.js"></script>
<script type="text/javascript">
	
	$(function(){

		$(".cadastrar").click(function(){
			//Locatário selecionado
			var idLocatario = $("#selecao-locatario").val();

			//Tamanho
			var tamanho = $("[name='tamanho']:checked").val();

			//Área
			var area = $("[name='area']:checked").val();

			//Tipo do imóvel
			var tipoDoImovel = $("[name='tipo']:checked").val();

			//Valores
			var aluguel = $("#aluguel").val();
			var condominio = $("#condominio").val();
			var iptu = $("#iptu").val();

			//Endereço
			var logradouro = $("#logradouro").val();
			var cidade = $("#cidade").val();
			var bairro = $("#bairro").val();
			var estado = $("#estado").val();
			var uf = $("#uf").val();
			var numero = $("#numero").val();
			var complemento = $("#complemento").val();
			var cep = $("#cep").val();


			var idImovel = $("#formulario").attr('data-id');
			var tipo = "imovel";
			var acao;
			if(idImovel == ''){
				acao = "cadastrar";
			} else {
				acao = "alterar";
			}

			$.post("/actControl.php", {"idLocatario":idLocatario, "tamanho":tamanho, "area":area, "tipoDoImovel":tipoDoImovel, "aluguel":aluguel, "condominio":condominio, "iptu":iptu, "logradouro":logradouro, "cidade":cidade, "bairro":bairro, "estado":estado, "uf":uf, "numero":numero, "complemento":complemento, "cep":cep, "idImovel":idImovel, "tipo": tipo, "acao":acao}, function(resposta){

				alert(resposta.mensagem);
				if(resposta.success == true){
					location.reload();
				}
			}, 'json');

		});

		$("#cep").blur(function(){
			var cep = $(this).val();
			if(cep != ""){
				pesquisaCEP(cep);
			}
		});
	});

	
</script>