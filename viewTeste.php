<?php
	require_once("util/autoload.php");

	//session_start();
	
	$administradorController = new AdministradorController();
	//$administrador = $administradorController->logar("luizfelipe", "luiz");

	//var_dump($_SESSION);
	//$administrador = $administradorController->obterTodos();

	//var_dump($administrador);die();
	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Cadastrar Administrador</title>
	</head>
	<body>
		<div id="formulario">
			<label for="nome">Nome:</label>
			<input type="text" name="nome" id="nome"></br></br>
			<label for="cpf">CPF:</label>
			<input type="text" name="cpf" id="cpf"></br></br>
			<label for="telefones">Telefones:</label>
			<input type="text" name="telefones[]" id="telefones"></br></br>
			<label for="funcao">Função:</label>
			<select name="funcao" id="funcao">
				<option value="Corretor">Corretor</option>
				<option value="Secretaria">Secretária</option>
				<option value="Diretor">Diretor</option>
			</select></br></br>
			<label for="email">Email:</label>
			<input type="text" name="email" id="email"></br></br>
			<label for="usuario">Usuário:</label>
			<input type="text" name="usuario" id="usuario"></br></br>
			<label for="senha">Senha:</label>
			<input type="password" name="senha" id="senha"></br></br>
			<input type="button" name="botao" id="botao" value="Cadastrar">
		</div>
	</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
	$(function(){

		$("#botao").click(function(){
			var nome = $('#nome').val();
			var cpf = $('#cpf').val();
			var telefones = [];
			telefones.push($('#telefones').val());
			var funcao = $('#funcao').val();
			var email = $('#email').val();
			var usuario = $('#usuario').val();
			var senha = $('#senha').val();
			var tipo = 'administrador';
			var acao = 'cadastrar';

			$.post("/actControl.php", { "nome":nome, "cpf":cpf, "telefones":telefones, "funcao":funcao, "email":email, "usuario":usuario, "senha":senha, "tipo":tipo, "acao":acao}, function(result){
				alert(result.mensagem);
			}, 'json');

		});
	});

</script>
