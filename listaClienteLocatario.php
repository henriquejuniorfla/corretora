<?php
	require_once("util/autoload.php");
	
	$locatarioController = new LocatarioController();
	$locatarios = $locatarioController->obterTodos();

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>HAM Corretora - Lista Clientes Locatários</title>
	<link rel="shortcut icon" href="img/logo.ico" type="image/x-icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="css/owl.theme.default.css" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="css/magnific-popup.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />

</head>

<body>
	<!-- Header -->
	<header>

		<!-- Nav -->
		<nav id="nav" class="navbar">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="index.html">
							<img class="logo" src="img/logo.png" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.html#home">Home</a></li>
					<li><a href="index.html#clientes">Clientes</a></li>
					<li><a href="index.html#imoveis">Imóveis</a></li>
					<li><a href="index.html#controle">Controle</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-9">
					<div class="blog">
						
						<!-- lista-clientes -->
						<div class="lista">
							<h4>Clientes Locatários</h4>

							<?php 
							if($locatarios == null){ ?>
								<p>Nenhum cliente cadastrado</p>
							<?php } else {
								foreach($locatarios as $locatario){ ?>
								<?php
									$telefones = $locatarioController->obterTelefones($locatario->getId());
									$numerosTelefones = "";
									if($telefones != null){
										foreach($telefones as $telefone){
											$numerosTelefones = $numerosTelefones . $telefone->getNumeros()[0] . " / ";
											
										}
									}
								?>
								<div class="media">
									<div class="media-body" data-id="<?php echo $locatario->getId() ?>">
										<h4 class="media-heading"><?php echo $locatario->getNome() ?><a href="#" class="reply"><i class="fa fa-edit"></i></a><a href="#" class="delete"><i class="fa fa-trash"></i></a></h4>
										<p>Email: <?php echo $locatario->getEmail() ?></p>
										<p>Telefone: <?php echo $numerosTelefones ?></p>
									</div>
								</div>
							<?php }} ?>

						</div>
						<!-- /lista-clientes -->

					</div>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->

	<!-- Footer -->
	<footer id="footer" class="sm-padding bg-dark">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<div class="col-md-12">

					<!-- footer logo -->
					<div class="footer-logo">
						<a href="index.html"><img src="img/logo-alt.png" alt="logo"></a>
					</div>
					<!-- /footer logo -->

					<!-- footer copyright -->
					<div class="footer-copyright">
						<p>Copyright © 2017. All Rights Reserved. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
					</div>
					<!-- /footer copyright -->

				</div>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular.js"></script>
	<script>
	var app = angular.module("App", []);
	app.controller("AppController", function($scope) {
		$scope.rows = [];

		$scope.add = function() {
			$scope.rows.push({tel:""});
		};
            
		$scope.remove = function(row) {
			var index = $scope.rows.indexOf(row);
			$scope.rows.splice(index, 1);
		};
	});
	</script>
</body>
</html>

<script type="text/javascript">
	$(function(){

		$(".delete").click(function(){
			var idLocatario = $(this).closest('div').attr('data-id');
			if(confirm("Deseja realmente excluir esse locatário?")){
				var tipo = 'locatario';
				var acao = 'excluir';
				$.post("/actControl.php", {"idLocatario":idLocatario, "tipo":tipo, "acao":acao}, function(resposta){
					alert(resposta.mensagem);
					if(resposta.success == true){
						location.reload();
					}
				}, 'json');
			}
		});

		$(".reply").click(function(){
			var idLocatario = $(this).closest('div').attr('data-id');
			location.href = "/cadastraLocatario.php?id="+idLocatario;
		});
	});
</script>
