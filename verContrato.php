<?php
	require_once("util/autoload.php");

	$aluguelController = new AluguelController();

	if(isset($_GET['aluguel']) && is_numeric($_GET['aluguel'])){
		$idAluguel = $_GET['aluguel'];
		$aluguel = $aluguelController->obterComId($idAluguel);

		$alugueisAtrasados = $aluguelController->obterPagamentosAtrasados($idAluguel);
		$mesesDeAtraso = "";
		$valorTotalAtrasado = 0;
		foreach($alugueisAtrasados as $aluguelAtrasado){
			$data = date("d/m/Y", strtotime($aluguelAtrasado['mesRef']));
			$mesesDeAtraso .= $data." - ";
			$valorTotalAtrasado += $aluguelAtrasado['valor'];
		}
	} else {
		die("ID do aluguel inválido");
	}
	
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>HAM Corretora - Gerenciar Visitação</title>
	<link rel="shortcut icon" href="img/logo.ico" type="image/x-icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="css/owl.theme.default.css" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="css/magnific-popup.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />
	<link type="text/css" rel="stylesheet" href="css/styleCadastro.css" />

</head>

<body>
	<!-- Header -->
	<header>

		<!-- Nav -->
		<nav id="nav" class="navbar">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="index.html">
							<img class="logo" src="img/logo.png" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.html#home">Home</a></li>
					<li><a href="index.html#clientes">Clientes</a></li>
					<li><a href="index.html#imoveis">Imóveis</a></li>
					<li><a href="index.html#controle">Controle</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog">
						<!-- form -->
						<div class="reply-form">
							<h3 class="title">Contrato de locação</h3>

							<input type="hidden" name="locador" id="locador" value="<?php echo $aluguel->getLocador()->getId()?>">
							<input type="hidden" name="imovel" id="imovel" value="<?php echo $aluguel->getImovel()->getId()?>">
							<input type="hidden" name="inicioContrato" id="inicioContrato" value="<?php echo $aluguel->getDataInicio()?>">


							<form id="formulario" data-id="<?php echo $aluguel->getId()?>">

								<h4>Cliente</h4>
								<p><?php echo $aluguel->getLocador()->getNome()?></p>

								<h4>Imóvel</h4>

								<div class="imovelinfo">
									<img src="img/casa1.jpg">
									<div class="col.md-2">
										<p>Endereço: <?php echo $aluguel->getImovel()->getEndereco()->getEnderecoCompleto();?></p>
										<p>CEP: <?php echo $aluguel->getImovel()->getEndereco()->getCep();?></p>
										
										<p>Tamanho: <?php echo Tamanho::toString($aluguel->getImovel()->getTamanho());?></p>
										<p>Tipo: <?php echo Tipo::toString($aluguel->getImovel()->getTipoDoImovel());?></p>

										<h4>Valor</h4>
										<p>Aluguel: R$ <?php echo $aluguel->getImovel()->getAluguel(true)?></p>
										<p>Condomínio: R$ <?php echo $aluguel->getImovel()->getCondominio(true)?></p>
										<p>IPTU: R$ <?php echo $aluguel->getImovel()->getIptu(true)?></p>

										<h4>Proprietário</h4>
										<p><?php echo $aluguel->getImovel()->getLocatario()->getNome()?></p>
									</div>
								</div>

								<br><div>
									<h5>Aluguéis atrasados</h5>
									<p>Meses de atraso: <?php echo $mesesDeAtraso ?></p>
									<p>Valor total: R$ <?php echo number_format($valorTotalAtrasado,2,",",".")?></p>
								</div>

								<br><div>
									<h5>Contrato</h5>
									<p>Início do Contrato: <?php echo date("d/m/Y", strtotime($aluguel->getDataInicio()));?></p>
									<p>Fim do Contrato: <?php echo date("d/m/Y", strtotime($aluguel->getDataFim()));?></p>
								</div>

								<div class="col-md-12 opcoesContrato">
									<button type="button" class="main-btn contrato">Imprimir Contrato</button>
									<button type="button" class="main-btn boletos">Imprimir Boletos</button>
								</div>

								<div class="col-md-12">							
									<button type="button" class="main-btn renovar">Renovar Contrato</button>
								</div>
							</form>
						</div>
						<!-- /form -->
					</div>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->

	<!-- Footer -->
	<footer id="footer" class="sm-padding bg-dark">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<div class="col-md-12">

					<!-- footer logo -->
					<div class="footer-logo">
						<a href="index.html"><img src="img/logo-alt.png" alt="logo"></a>
					</div>
					<!-- /footer logo -->

					<!-- footer copyright -->
					<div class="footer-copyright">
						<p>Copyright © 2017. All Rights Reserved. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
					</div>
					<!-- /footer copyright -->

				</div>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>

<script type="text/javascript">
	
	$(function(){

		$(".renovar").click(function(){
			var idAluguel = $("#formulario").attr('data-id');
			var tipo = "aluguel";
			var acao = "renovarContrato";

			if(confirm("Deseja realmente renovar o contrato de aluguel?")){
				$.post("/actControl.php", {"aluguel":idAluguel, "tipo":tipo, "acao":acao}, function(resposta){
					alert(resposta.mensagem);
					if(resposta.success == true){
						location.reload();
					}
				}, 'json');
			}
		});

		$(".contrato").click(function(){
			var idLocador = $("#locador").val();
			var idImovel = $("#imovel").val();
			var dataInicioContrato = $("#inicioContrato").val();

			if(dataInicioContrato != ""){
				var novaPagina = window.open("/impressao/contrato.php?locador="+idLocador+"&imovel="+idImovel+"&dataInicio="+dataInicioContrato, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");

				$(novaPagina).ready(function() { 
	                novaPagina.print();
	            });
	        } else {
	        	alert("Informe a data de ínicio do contrato");
	        }
		});

		$(".boletos").click(function(){
			var idLocador = $("#locador").val();
			var idImovel = $("#imovel").val();
			var dataInicioContrato = $("#inicioContrato").val();

			if(dataInicioContrato != ""){
				var novaPagina = window.open("/impressao/boletos.php?locador="+idLocador+"&imovel="+idImovel+"&dataInicio="+dataInicioContrato, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");

				$(novaPagina).ready(function() { 
	                novaPagina.print();
	            });
	        } else {
	        	alert("Informe a data de ínicio do contrato");
	        }
		});
	});
</script>