<?php
	header('Content-Type: application/json');
	session_start();

	require_once("util/autoload.php");

	$tipo = $_POST['tipo']; //ver depois se será enviado via GET ou POST 
	$acao = $_POST['acao']; //ver depois se será enviado via GET ou POST 

	try{
		if(isset($tipo) && isset($acao)){
			$tipo = trim(ucfirst($tipo));
			$controller = $tipo.'Controller';
			$controller = new $controller();

			$result = $controller->$acao();

			$resposta = new Resposta($result['success'], $result['mensagem'], $result['dados']);
			
		} else {
			$resposta = new Resposta(false, 'Não foi enviado o tipo ou ação da controladora!', null);
		}

	}catch(Exception $e){
		$resposta = new Resposta(false, "Erro: ".$e->getMessage(), null);
	}

	echo JSON::getResposta($resposta);

?>