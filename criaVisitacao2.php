<?php
	require_once("util/autoload.php");

	$idLocador = $_GET['locador'];
	$tamanho = $_GET['tamanho'];
	$area = $_GET['area'];
	$tipo = $_GET['tipo'];
	$valor = $_GET['valor'];

	$restricoes = array(
		"tamanho" => $tamanho,
		"area" => $area,
		"tipo" => $tipo
	);

	$imovelController = new ImovelController();
	$imoveis = $imovelController->obterComRestricao($restricoes);

	$aluguelController = new AluguelController();
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>HAM Corretora - Cadastrar Imóvel</title>
	<link rel="shortcut icon" href="img/logo.ico" type="image/x-icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="css/owl.theme.default.css" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="css/magnific-popup.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />
	<link type="text/css" rel="stylesheet" href="css/styleCadastro.css" />

</head>

<body>
	<!-- Header -->
	<header>

		<!-- Nav -->
		<nav id="nav" class="navbar">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="index.html">
							<img class="logo" src="img/logo.png" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.html#home">Home</a></li>
					<li><a href="index.html#clientes">Clientes</a></li>
					<li><a href="index.html#imoveis">Imóveis</a></li>
					<li><a href="index.html#controle">Controle</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Aside -->
				<aside id="aside" class="col-md-3">

					<h3 class="title">Criar Visitação</h3>

					<form id="formulario" action="criaVisitacao2.php">

								<div class="widget">
									<h4>Tamanho</h4>
									<div class="widget-category ">
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::CONJUGADO;?>" <?php if(isset($tamanho) && Tamanho::CONJUGADO == $tamanho) echo 'checked';?> > <?php echo Tamanho::toString(Tamanho::CONJUGADO); ?></span>
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::UM_QUARTO;?>" <?php if(isset($tamanho) && Tamanho::UM_QUARTO == $tamanho) echo 'checked';?> > <?php echo Tamanho::toString(Tamanho::UM_QUARTO); ?></span>
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::DOIS_QUARTO;?>" <?php if(isset($tamanho) && Tamanho::DOIS_QUARTO == $tamanho) echo 'checked';?> > <?php echo Tamanho::toString(Tamanho::DOIS_QUARTO); ?></span>
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::TRES_QUARTO;?>" <?php if(isset($tamanho) && Tamanho::TRES_QUARTO == $tamanho) echo 'checked';?> > <?php echo Tamanho::toString(Tamanho::TRES_QUARTO); ?></span>
										<span><input type="radio" name="tamanho" value="<?php echo Tamanho::QUATRO_OU_MAIS_QUARTOS;?>" <?php if(isset($tamanho) && Tamanho::QUATRO_OU_MAIS_QUARTOS == $tamanho) echo 'checked';?> > <?php echo Tamanho::toString(Tamanho::QUATRO_OU_MAIS_QUARTOS); ?></span>
									</div>
								</div>

								<div class="widget">
									<h4>Área</h4>
									<div class="widget-category ">
										<span><input type="radio" name="area" value="<?php echo Area::CENTRO;?>" <?php if(isset($area) && Area::CENTRO == $area) echo 'checked';?> > <?php echo Area::toString(Area::CENTRO); ?></span>
										<span><input type="radio" name="area" value="<?php echo Area::ZONA_NORTE;?>" <?php if(isset($area) && Area::ZONA_NORTE == $area) echo 'checked';?> > <?php echo Area::toString(Area::ZONA_NORTE); ?></span>
										<span><input type="radio" name="area" value="<?php echo Area::ZONA_SUL;?>" <?php if(isset($area) && Area::ZONA_SUL == $area) echo 'checked';?> > <?php echo Area::toString(Area::ZONA_SUL); ?></span>
										<span><input type="radio" name="area" value="<?php echo Area::ZONA_OESTE;?>" <?php if(isset($area) && Area::ZONA_OESTE == $area) echo 'checked';?> > <?php echo Area::toString(Area::ZONA_OESTE); ?></span>
									</div>
								</div>

								<div id="valor" class="widget">
									<h4>Valor</h4>
									<div class="widget-category ">
										<span><input type="radio" name="valor" value="ate300" checked> Até R$ 300,00</span>
										<span><input type="radio" name="valor" value="precomedio"> R$ 300,00 - R$ 1000,00</span>
										<span><input type="radio" name="valor" value="acimade1000"> Acima de R$ 1000,00</span>
										<span>De: R$ <input type="text" class="preco"></span>
										<span>Até: R$ <input type="text" class="preco"></span>
									</div>
								</div>

								<div class="widget">
									<h4>Tipo</h4>
									<div class="widget-category ">
										<span><input type="radio" name="tipo" value="<?php echo Tipo::CASA; ?>" <?php if(isset($tipo) && Tipo::CASA == $tipo) echo 'checked';?> > <?php echo Tipo::toString(Tipo::CASA); ?></span>
										<span><input type="radio" name="tipo" value="<?php echo Tipo::APARTAMENTO; ?>" <?php if(isset($tipo) && Tipo::APARTAMENTO == $tipo) echo 'checked';?>> <?php echo Tipo::toString(Tipo::APARTAMENTO); ?></span>
									</div>
								</div><br>

								<div class="col-md-12">
									<button type="button" class="main-btn">Procurar</button>
								</div>
					</form>

				</aside>
				<!-- /Aside -->

				<!-- Main -->
				<main id="main" class="col-md-9">
					<div class="blog">
						
						<h3 class="title">Criar Visitação</h3>

						<input type="hidden" name="locador" id="locador" value="<?php echo $idLocador ?>">

						<div id="galery-imoveis">
							<div class="row">
								<?php 
									$contador = 0;
									$imoveisPorLinha = 3;
									$contImg = 1;
									if($imoveis == null || count($imoveis) == 0){
										echo "<p> Não há imóveis disponíveis para estas seleções </p>";
									} else {
										foreach($imoveis as $imovel){ 
											$aluguel = $aluguelController->obterComIdImovel($imovel->getId());
											?>
											<div class="imovel col-med-2">

												<img src="img/casa<?php echo $contImg ?>.jpg">
												<p><?php echo $imovel->getEndereco()->getEnderecoCompleto();?></p>
												<p>CEP: <?php echo $imovel->getEndereco()->getCep();?></p>
												<p>Tamanho: <?php echo Tamanho::toString($imovel->getTamanho());?></p>
												<p>Tipo: <?php echo Tipo::toString($imovel->getTipoDoImovel());?></p>
												<h5>Valor: R$ <?php echo $imovel->getAluguel(true)?></h5>
												<?php
													if($aluguel == null){ ?>
														<a href="verImovel.php?id=<?php echo $imovel->getId()?>&locador=<?php echo $idLocador?>"><button type="button" class="main-btn">Ver</button></a>
												<?php } else { ?>
													<h4 style="color:red;">ALUGADO</h4>
												<?php } ?>
											</div>
									<?php
											$contImg++;
											$contador++;

											if($contador == $imoveisPorLinha){
												echo "</div>";
												echo "<div class='row'>";
											}
										}
									}
								?>
							</div>
						</div>
					</div>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->

	<!-- Footer -->
	<footer id="footer" class="sm-padding bg-dark">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<div class="col-md-12">

					<!-- footer logo -->
					<div class="footer-logo">
						<a href="index.html"><img src="img/logo-alt.png" alt="logo"></a>
					</div>
					<!-- /footer logo -->

					<!-- footer copyright -->
					<div class="footer-copyright">
						<p>Copyright © 2017. All Rights Reserved. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
					</div>
					<!-- /footer copyright -->

				</div>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>

<script type="text/javascript">
	
	$(function(){
		$(".main-btn").click(function(){
			//Locador
			var idLocador = $("#locador").val();

			//Tamanho
			var tamanho = $("[name='tamanho']:checked").val();

			//Área
			var area = $("[name='area']:checked").val();

			//Tipo do imóvel
			var tipoDoImovel = $("[name='tipo']:checked").val();

			//Valor
			var valor = $("[name='valor']:checked").val();

			location.href = "/criaVisitacao2.php?locador="+idLocador+"&tamanho="+tamanho+"&area="+area+"&valor="+valor+"&tipo="+tipoDoImovel;
		});
	});
</script>