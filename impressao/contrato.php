<?php
	require_once("../util/autoload.php");
	
	$imovelController = new ImovelController();
	$imovel = null;
	if(isset($_GET['imovel'])){
		$imovel = $imovelController->obterComId($_GET['imovel']);
	}

	$locador = null;
	if(isset($_GET['locador'])){
		$idLocador = $_GET['locador'];
		$locadorController = new LocadorController();
		$locador = $locadorController->obterComId($idLocador);
	}

	if($imovel == null || $locador == null){
		die("Imóvel ou cliente inválidos");
	}

	if(!isset($_GET['dataInicio']) || empty($_GET['dataInicio'])){
		die("Informe a data de início do contrato");
	} else {
		$prazoLocacao = 1; //anos;
		$dataInicioContrato = date('Y-m-d', strtotime($_GET['dataInicio']));
		$dataFinalContrato = date('Y-m-d', strtotime("+ $prazoLocacao year $dataInicioContrato"));
		//formatando data
		$dataInicioContrato = date('d/m/Y', strtotime($dataInicioContrato));
		$dataFinalContrato = date('d/m/Y', strtotime($dataFinalContrato));
	}

	$locatario = $imovel->getLocatario();

	$diaAtual = date('d');
	$mesAtual = Util::mesPorExtenso(date('m')); //por extenso
	$anoAtual = date('Y');
	$dataAtual = $diaAtual." de ".$mesAtual." de ".$anoAtual;
	
?>

<!DOCTYPE html>
<html>
	<head>
		<div style="width:150px;"><img src="../img/logo.png" alt="HAM Corretora" width="100" height="40" title="HAM Corretora" /></div>
		<title>Contrato de Aluguel</title>
	</head>
	<body>
		<h2>Contrato de Aluguel</h2>

		<p><b>LOCADOR:</b> <?php echo $locador->getNome()?>, portador da cédula de identidade R.G. nº <?php echo $locador->getRg()?> e CPF/MF nº <?php echo $locador->getCpf()?>.</p>

		<p><b>LOCATÁRIO:</b> <?php echo $locatario->getNome()?>, portador da cédula de identidade R.G. nº <?php echo $locatario->getRg()?> e CPF/MF nº <?php echo $locatario->getCpf()?>.</p>

		<p>CLÁUSULA PRIMEIRA: O objeto deste contrato de locação é o imóvel residencial, situado em <?php echo $imovel->getEndereco()->getEnderecoCompleto()?>.</p>

		<p>CLÁUSULA SEGUNDA: O prazo da locação é de <?php echo $prazoLocacao ?> ano, iniciando-se em <?php echo $dataInicioContrato ?> com término em <?php echo $dataFinalContrato ?>, independentemente e aviso, notificação ou interpelação judicial ou mesmo extrajudicial.</p>

		<p>CLÁUSULA TERCEIRA: O aluguel mensal, deverá ser pago até a data de vencimento, no local indicado pelo LOCATÁRIO, é de R$ <?php echo $imovel->getAluguel(true)?> (Valor) mensais, reajustados anualmente, de conformidade com a variação do IGP-M apurada no ano anterior, e na sua falta, por outro índice criado pelo Governo Federal e, ainda, em sua substituição, pela Fundação Getúlio Vargas, reajustamento este sempre incidente e calculado sobre o último aluguel pago no último mês do ano anterior.</p>

		<p>CLÁUSULA QUARTA: O LOCADOR será responsável por todos os tributos incidentes sobre o imóvel bem como despesas ordinárias de condomínio, e quaisquer outras despesas que recaírem sobre o imóvel, arcando tambem com as despesas provenientes de sua utilização seja elas, ligação e consumo de luz, força, água e gás que serão pagas diretamente às empresas concessionárias dos referidos serviços.</p>

		<p>CLÁUSULA QUINTA: Em caso demora no pagamento do aluguel, será aplicada multa de 2% (dois por cento) sobre o valor devido e juros mensais de 1% (um por cento) do montante devido.</p>

		<p>CLÁUSULA SEXTA: Fica ao LOCADOR, a responsabilidade em zelar pela conservação, limpeza do imóvel, efetuando as reformas necessárias para sua manutenção sendo que os gastos e pagamentos decorrentes da mesma, correrão por conta do mesmo. O LOCADOR está obrigado a devolver o imóvel em perfeitas condições de limpeza, conservação e pintura, quando finda ou rescindida esta avença, conforme constante no termo de vistoria em anexo. O LOCADOR não poderá realizar obras que alterem ou modifiquem a estrutura do imóvel locado, sem prévia autorização por escrito do LOCATÁRIO. Caso este consinta na realização das obras, estas ficarão desde logo, incorporadas ao imóvel, sem que assista ao LOCADOR qualquer indenização pelas obras ou retenção por benfeitorias. As benfeitorias removíveis poderão ser retiradas, desde que não desfigurem o imóvel locado.</p>

		<p>PARÁGRAFO ÚNICO: O LOCADOR declara receber o imóvel em perfeito estado de conservação e perfeito funcionamento devendo observar o que consta no termo de vistoria.</p>

		<p>CLÁUSULA SÉTIMA: O LOCADOR declara, que o imóvel ora locado, destina-se única e exclusivamente para o seu uso residencial e de sua família.</p>

		<p>PARÁGRAFO ÚNICO: O LOCADOR, obriga por si e sua família, a cumprir e a fazer cumprir integralmente as disposições legais sobre o Condomínio, a sua Convenção e o seu Regulamento Interno.</p>

		<p>CLÁUSULA OITAVA: O LOCADOR não poderá sublocar, transferir ou ceder o imóvel, sendo nulo de pleno direito qualquer ato praticado com este fim sem o consentimento prévio e por escrito do LOCATÁRIO.</p>

		<p>CLÁUSULA NONA: Em caso de sinistro parcial ou total do prédio, que impossibilite a habitação o imóvel locado, o presente contrato estará rescindido, independentemente de aviso ou interpelação judicial ou extrajudicial; no caso de incêndio parcial, obrigando a obras de reconstrução, o presente contrato terá suspensa a sua vigência e reduzida a renda do imóvel durante o período da reconstrução à metade do que na época for o aluguel, e sendo após a reconstrução devolvido o LOCADOR pelo prazo restante do contrato, que ficará prorrogado pelo mesmo tempo de duração das obras de reconstrução.</p>

		<p>CLÁUSULA DÉCIMA : Em caso de desapropriação total ou parcial do imóvel locado, ficará rescindido de pleno direito o presente contrato de locação, independente de quaisquer indenizações de ambas as partes ou contratantes.</p>

		<p>CLÁUSULA DÉCIMA PRIMEIRA: Falecendo o FIADOR, o LOCADOR, em 30 (trinta) dias, dar substituto idôneo que possa garantir o valor locativo e encargos do referido imóvel, ou prestar seguro fiança de empresa idônea.</p>

		<p>CLÁUSULA DÉCIMA SEGUNDA: No caso de alienação do imóvel, obriga-se o LOCADOR, dar preferência ao LOCADOR, e se o mesmo não utilizar-se dessa prerrogativa, o LOCADOR deverá constar da respectiva escritura pública, a existência do presente contrato, para que o adquirente o respeite nos termos da legislação vigente.</p>

		<p>CLÁUSULA DÉCIMA TERCEIRA: O FIADOR e principal pagador do LOCADOR, responde solidariamente por todos os pagamentos descritos neste contrato bem como, não só até o final de seu prazo, como mesmo depois, até a efetiva entrega das chaves ao LOCATÁRIO e termo de vistoria do imóvel.</p>

		<p>CLÁUSULA DÉCIMA QUARTA: É facultado ao LOCATÁRIO vistoriar, por si ou seus procuradores, sempre que achar conveniente, para a certeza do cumprimento das obrigações assumidas neste contrato.</p>

		<p>CLÁUSULA DÉCIMA QUINTA: A infração de qualquer das cláusulas do presente contrato, sujeita o infrator à multa de duas vezes o valor do aluguel, tomando-se por base, o último aluguel vencido.</p>

		<p>CLÁUSULA DÉCIMA SEXTA: As partes contratantes obrigam-se por si, herdeiros e/ou sucessores, elegendo o Foro da Cidade do (colocar o fórum do município), para a propositura de qualquer ação.</p>

		<p>E, por assim estarem justos e contratados, mandaram extrair o presente instrumento em três (03) vias, para um só efeito, assinando-as, juntamente com as testemunhas, a tudo presentes.</p>

		<br>
		<p>Nova Friburgo-RJ, <?php echo $dataAtual ?></p>
		<br>

		<p>________________________________</p>
		<p>LOCADOR (<?php echo $locador->getNome()?>)</p>
		<br>
		<p>________________________________</p>
		<p>LOCATÁRIO (<?php echo $locatario->getNome()?>)</p>
		<br>
		<p>________________________________</p>
		<p>FIADOR</p>
		<br>
		<p>________________________________</p>
		<p>TESTEMUNHA</p>
	</body>
</html>