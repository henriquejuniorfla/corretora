<?php
	require_once("util/autoload.php");

	$visitacaoController = new VisitacaoController();
	$visitacoes = $visitacaoController->obterTodos();

	$aluguelController = new AluguelController();

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>HAM Corretora - Gerenciar Visitação</title>
	<link rel="shortcut icon" href="img/logo.ico" type="image/x-icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="css/owl.theme.default.css" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="css/magnific-popup.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />
	<link type="text/css" rel="stylesheet" href="css/styleCadastro.css" />

</head>

<body>
	<!-- Header -->
	<header>

		<!-- Nav -->
		<nav id="nav" class="navbar">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="index.html">
							<img class="logo" src="img/logo.png" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.html#home">Home</a></li>
					<li><a href="index.html#clientes">Clientes</a></li>
					<li><a href="index.html#imoveis">Imóveis</a></li>
					<li><a href="index.html#controle">Controle</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-9">
					<div class="blog">
						<!-- lista-visitacao -->
						<div class="lista">
							<h4>Visitações</h4><br>

							<?php
								$dataAtual = date('Y-m-d');
								if($visitacoes == null){
									echo "<p> Não há visitações agendadas </p>";
								} else { 
									foreach($visitacoes as $visitacao){ 
										$aluguel = $aluguelController->obterComIdImovel($visitacao->getImovel()->getId());
										?>
										<div class="media" data-id="<?php echo $visitacao->getId() ?>">
											<div class="media-body" data-id="x">
												<h4 class="media-heading"><?php echo $visitacao->getLocador()->getNome() ?>
													<?php 
														if($aluguel == null){ ?>
															<a href="criaAluguel.php?id=<?php echo $visitacao->getImovel()->getId()?>&locador=<?php echo $visitacao->getLocador()->getId()?>" class="reply">
															<i class="fa fa-check"></i>
															</a>
													<?php } ?>
													<a href="#" class="delete">
														<i class="fa fa-trash"></i>
													</a>
												</h4>
												<p>Endereço do Imóvel: <?php echo $visitacao->getImovel()->getEndereco()->getEnderecoCompleto() ?></p>

												<?php
													$dataDevolucaoChave = strtotime($visitacao->getDataDevolucaoChave());
													$dataDevolucaoFormatada = date('d/m/Y', $dataDevolucaoChave);
													if($dataAtual > $dataDevolucaoChave){
														echo "<p>Data de Entrega da Chave: <span style='color: red;'>".$dataDevolucaoFormatada."</span></p>";
													} else {
														echo "<p>Data de Entrega da Chave: ".$dataDevolucaoFormatada."</p>";
													}

													if($aluguel != null){
														echo "<h4 style='color:red;'> Imóvel ALUGADO </h4>";
													}
												?>
											</div>
										</div>
								<?php }
							} ?>
						</div>
						<!-- /lista-visitacao -->
					</div>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->

	<!-- Footer -->
	<footer id="footer" class="sm-padding bg-dark">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<div class="col-md-12">

					<!-- footer logo -->
					<div class="footer-logo">
						<a href="index.html"><img src="img/logo-alt.png" alt="logo"></a>
					</div>
					<!-- /footer logo -->

					<!-- footer copyright -->
					<div class="footer-copyright">
						<p>Copyright © 2017. All Rights Reserved. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
					</div>
					<!-- /footer copyright -->

				</div>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>

<script type="text/javascript">
	$(function(){

		$(".delete").click(function(){
			var idVisitacao = $(this).closest('div').parent().attr('data-id');

			if(confirm("Deseja confirmar a devolução da chave deste imóvel?")){
				var tipo = 'visitacao';
				var acao = 'marcarComoDisponivel';
				$.post("/actControl.php", {"idVisitacao":idVisitacao, "tipo":tipo, "acao":acao}, function(resposta){
						location.reload();
				}, 'json');
			}
		});
	});
</script>