<?php

	$raiz = $_SERVER['DOCUMENT_ROOT'];

	$arrayClasses = array(
		"$raiz/util/Util",
		"$raiz/util/Resposta",
		"$raiz/util/JSON",
		"$raiz/classes/ConexaoSingleton",
		"$raiz/classes/BancoDados",
		"$raiz/classes/Controller",
		"$raiz/classes/Service",
		"$raiz/classes/Dao",
		"$raiz/classes/Enum",
		"$raiz/classes/cliente/Cliente",
		"$raiz/classes/administrador/Administrador",
		"$raiz/classes/administrador/AdministradorController",
		"$raiz/classes/administrador/AdministradorService",
		"$raiz/classes/administrador/AdministradorDao",
		"$raiz/classes/aluguel/Aluguel",
		"$raiz/classes/aluguel/AluguelController",
		"$raiz/classes/aluguel/AluguelService",
		"$raiz/classes/aluguel/AluguelDao",
		"$raiz/classes/aluguel/Pagamento",
		"$raiz/classes/area/Area",
		"$raiz/classes/estado/Estado",
		"$raiz/classes/estado/EstadoController",
		"$raiz/classes/estado/EstadoService",
		"$raiz/classes/estado/EstadoDao",
		"$raiz/classes/cidade/Cidade",
		"$raiz/classes/cidade/CidadeController",
		"$raiz/classes/cidade/CidadeService",
		"$raiz/classes/cidade/CidadeDao",
		"$raiz/classes/bairro/Bairro",
		"$raiz/classes/bairro/BairroController",
		"$raiz/classes/bairro/BairroService",
		"$raiz/classes/bairro/BairroDao",
		"$raiz/classes/endereco/Endereco",
		"$raiz/classes/endereco/EnderecoController",
		"$raiz/classes/endereco/EnderecoService",
		"$raiz/classes/endereco/EnderecoDao",
		"$raiz/classes/imovel/Imovel",
		"$raiz/classes/imovel/ImovelController",
		"$raiz/classes/imovel/ImovelService",
		"$raiz/classes/imovel/ImovelDao",
		"$raiz/classes/locador/Locador",
		"$raiz/classes/locador/LocadorController",
		"$raiz/classes/locador/LocadorService",
		"$raiz/classes/locador/LocadorDao",
		"$raiz/classes/locatario/Locatario",
		"$raiz/classes/locatario/LocatarioController",
		"$raiz/classes/locatario/LocatarioService",
		"$raiz/classes/locatario/LocatarioDao",
		"$raiz/classes/tamanho/Tamanho",
		"$raiz/classes/telefone/Telefone",
		"$raiz/classes/tipo/Tipo",
		"$raiz/classes/visitacao/Visitacao",
		"$raiz/classes/visitacao/VisitacaoController",
		"$raiz/classes/visitacao/VisitacaoService",
		"$raiz/classes/visitacao/VisitacaoDao"
	);

	function __autoload($nomeClasse){
		include($nomeClasse.'.php');
	}

	foreach($arrayClasses as $nomeClasse){
		__autoload($nomeClasse);
	}
?>