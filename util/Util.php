<?php

abstract class Util{


	public static function validaCPF($cpf = null) {
 
	    // Verifica se um número foi informado
	    if(empty($cpf)) {
	        return false;
	    }
	 
	    // Elimina possivel mascara
	    $cpf = preg_replace('/[^0-9]/', '', $cpf);
	    //$cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
	     
	    // Verifica se o numero de digitos informados é igual a 11 
	    if (strlen($cpf) != 11) {
	        return false;
	    }
	    // Verifica se nenhuma das sequências invalidas abaixo 
	    // foi digitada. Caso afirmativo, retorna falso
	    else if ($cpf == '00000000000' || 
	        $cpf == '11111111111' || 
	        $cpf == '22222222222' || 
	        $cpf == '33333333333' || 
	        $cpf == '44444444444' || 
	        $cpf == '55555555555' || 
	        $cpf == '66666666666' || 
	        $cpf == '77777777777' || 
	        $cpf == '88888888888' || 
	        $cpf == '99999999999') {
	        return false;
	     // Calcula os digitos verificadores para verificar se o
	     // CPF é válido
	     } else {   
	         
	        for ($t = 9; $t < 11; $t++) {   
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf{$c} * (($t + 1) - $c);
	            }

	            $d = ((10 * $d) % 11) % 10;
	            if ((int)$cpf{$c} != $d) {
	                return false;
	            }
	        }
	 
	        return true;
	    }
	}


	public static function validaEmail($email) {

		if(filter_var($email, FILTER_VALIDATE_EMAIL)){
			return true;
		} else {
			return false;
		}
	}

	public static function validaNomeUsuario($nomeUsuario){
		$resposta = true;

		$caracteresInvalidos = array('á','é','í','ó','ú','Á','É','Í','Ó','Ú','ã','õ','Ã','Õ','ç','Ç','~','!','#','@','$','%','^','&','*','(',')','_','+','=','.','-');

		foreach($caracteresInvalidos as $caractere){
			str_replace($caractere, 'achou', $nomeUsuario, $qtdModificacao);
			if($qtdModificacao > 0){
				$resposta = false;
				break;
			}
		}
		return $resposta;
	}

	public static function soNumeros($string){
		return preg_replace("/[^0-9]/", "", $string);
	}

	public static function mesPorExtenso($mes){
		switch($mes){
			case '1': 
			case '01': return "Janeiro";
			case '2': 
			case '02': return "Fevereiro";
			case '3': 
			case '03': return "Março";
			case '4': 
			case '04': return "Abril";
			case '5': 
			case '05': return "Maio";
			case '6': 
			case '06': return "Junho";
			case '7': 
			case '07': return "Julho";
			case '8': 
			case '08': return "Agosto";
			case '9': 
			case '09': return "Setembro";
			case '10': return "Outubro";
			case '11': return "Novembro";
			case '12': return "Dezembro";
		}
	}

}
?>