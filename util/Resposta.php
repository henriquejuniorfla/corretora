<?php

class Resposta{

	private $success = "";
	private $mensagem = "";
	private $dados = "";

	function __construct($success, $mensagem, $dados = null){
		$this->success = $success;
		$this->mensagem = $mensagem;
		$this->dados = $dados;
	}

	public function getSuccess(){
		return $this->success;
	}

	public function getMensagem(){
		return $this->mensagem;
	}

	public function getDados(){
		return $this->dados;
	}
}
?>