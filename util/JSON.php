<?php

class JSON{

	function __construct(){}

	public static function getResposta(Resposta $objResposta){
		$arrayResposta = array(
			"success" => $objResposta->getSuccess(),
			"mensagem" => $objResposta->getMensagem(),
			"dados" => $objResposta->getDados()
		);
		return json_encode($arrayResposta);
	}

}
?>