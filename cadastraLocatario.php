<?php
	require_once("util/autoload.php");

	$locatario = null;
	$telefones = null;
	
	if(isset($_GET['id']) && is_numeric($_GET['id'])){
		$idLocatario = $_GET['id'];
		$locatarioController = new LocatarioController();
		$locatario = $locatarioController->obterComId($idLocatario);
		if($locatario != null)
			$telefones = $locatarioController->obterTelefones($locatario->getId());
	}

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>HAM Corretora - Cadastrar Cliente</title>
	<link rel="shortcut icon" href="img/logo.ico" type="image/x-icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="css/owl.theme.default.css" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="css/magnific-popup.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />
	<link type="text/css" rel="stylesheet" href="css/styleCadastro.css" />

</head>

<body>
	<!-- Header -->
	<header>

		<!-- Nav -->
		<nav id="nav" class="navbar">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="index.html">
							<img class="logo" src="img/logo.png" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.html#home">Home</a></li>
					<li><a href="index.html#clientes">Clientes</a></li>
					<li><a href="index.html#imoveis">Imóveis</a></li>
					<li><a href="index.html#controle">Controle</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-9">
					<div class="blog">
						<!-- form -->
						<div class="reply-form">
							<h3 class="title">Cadastrar Cliente Locatário</h3>
							<form id="formulario" data-id="<?php if(isset($locatario) && $locatario->getId() != null) echo $locatario->getId(); ?>">
								<input id="nome" class="input" type="text" placeholder="Nome" value="<?php if(isset($locatario) && $locatario->getNome() != null) echo $locatario->getNome(); ?>" required><br>
								<input id="cpf" class="input" type="text" placeholder="CPF" value="<?php if(isset($locatario) && $locatario->getCpf() != null) echo $locatario->getCpf(); ?>" <?php if(isset($locatario) && $locatario->getCpf() != null) echo 'readonly=true' ?> required>
								<input id="rg" class="input" type="text" placeholder="RG" value="<?php if(isset($locatario) && $locatario->getRg() != null) echo $locatario->getRg(); ?>" required>
								<input id="email" class="input" type="email" placeholder="Email" value="<?php if(isset($locatario) && $locatario->getEmail() != null) echo $locatario->getEmail(); ?>" required>
								<div id="telefones" ng-app="App" ng-controller="AppController">
									<?php
										if($telefones != null){
											foreach($telefones as $telefone){ ?>
												<input class="input telefones" type="text" placeholder="Telefone" value="<?php echo $telefone->getNumeros()[0]?>" required><br>							
											<?php }
										} else { ?>
											<input class="input telefones" type="text" placeholder="Telefone" required>
									<?php } ?>
									
  									<a class="main-btn"><i id="add-tel" class="fa fa-plus" ng-click="add();"></i></a><br>
  									<div ng-repeat="row in rows">
  										<input type="text" class="input telefones" placeholder="Telefone" ng-model="row.tel">
  										<a href="javascript:;" ng-click="remove(row);"><i class="fa fa-times"></i></a>
  									</div>
								</div>
								<h4>Endereço</h4><br>
								<input id="cep" class="input" type="text" placeholder="CEP" value="<?php if(isset($locatario) && $locatario->getEndereco() != null && $locatario->getEndereco()->getCep() != null) echo $locatario->getEndereco()->getCep(); ?>" required><br>
								<input id="logradouro" class="input" type="text" placeholder="Logradouro" value="<?php if(isset($locatario) && $locatario->getEndereco() != null && $locatario->getEndereco()->getLogradouro() != null) echo $locatario->getEndereco()->getLogradouro(); ?>" required>
								<input id="bairro" class="input" type="text" placeholder="Bairro" value="<?php if(isset($locatario) && $locatario->getEndereco() != null && $locatario->getEndereco()->getBairro() != null && $locatario->getEndereco()->getBairro()->getBairro() != null) echo $locatario->getEndereco()->getBairro()->getBairro(); ?>" required>
								<input id="cidade" class="input" type="text" placeholder="Cidade" value="<?php if(isset($locatario) && $locatario->getEndereco() != null && $locatario->getEndereco()->getBairro()->getCidade() != null && $locatario->getEndereco()->getBairro()->getCidade()->getCidade() != null) echo $locatario->getEndereco()->getBairro()->getCidade()->getCidade(); ?>" required>
								<input id="estado" class="input" type="text" placeholder="Estado" value="<?php if(isset($locatario) && $locatario->getEndereco() != null && $locatario->getEndereco()->getBairro()->getCidade()->getEstado() != null && $locatario->getEndereco()->getBairro()->getCidade()->getEstado()->getEstado() != null) echo $locatario->getEndereco()->getBairro()->getCidade()->getEstado()->getEstado(); ?>" required>
								<input id="uf" class="input" type="text" placeholder="UF" value="<?php if(isset($locatario) && $locatario->getEndereco() != null && $locatario->getEndereco()->getBairro()->getCidade()->getEstado() != null && $locatario->getEndereco()->getBairro()->getCidade()->getEstado()->getUf() != null) echo $locatario->getEndereco()->getBairro()->getCidade()->getEstado()->getUf(); ?>" required>
								<input id="numero" class="input" type="number" placeholder="Número" min="0" value="<?php if(isset($locatario) && $locatario->getEndereco() != null && $locatario->getEndereco()->getNumero() != null) echo $locatario->getEndereco()->getNumero(); ?>">
								<input id="complemento" class="input" type="text" placeholder="Complemento" value="<?php if(isset($locatario) && $locatario->getEndereco() != null && $locatario->getEndereco()->getComplemento() != null) echo $locatario->getEndereco()->getComplemento(); ?>">
								<h4>Conta Bancária</h4><br>
								<input id="conta" class="input" type="text" placeholder="Conta" value="<?php if(isset($locatario) && $locatario->getConta() != null) echo $locatario->getConta(); ?>" required><br>
								<button type="button" class="main-btn cadastrar">Cadastrar</button>
							</form>
						</div>
						<!-- /form -->
					</div>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->

	<!-- Footer -->
	<footer id="footer" class="sm-padding bg-dark">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<div class="col-md-12">

					<!-- footer logo -->
					<div class="footer-logo">
						<a href="index.html"><img src="img/logo-alt.png" alt="logo"></a>
					</div>
					<!-- /footer logo -->

					<!-- footer copyright -->
					<div class="footer-copyright">
						<p>Copyright © 2017. All Rights Reserved. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
					</div>
					<!-- /footer copyright -->

				</div>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular.js"></script>
	<script>
	var app = angular.module("App", []);
	app.controller("AppController", function($scope) {
		$scope.rows = [];

		$scope.add = function() {
			$scope.rows.push({tel:""});
		};
            
		$scope.remove = function(row) {
			var index = $scope.rows.indexOf(row);
			$scope.rows.splice(index, 1);
		};
	});
	</script>
</body>
</html>

<script type="text/javascript" src="js/endereco.js"></script>
<script type="text/javascript">
	
	$(function(){

		$(".cadastrar").click(function(){
			var nome = $("#nome").val();
			var cpf = $("#cpf").val();
			var rg = $("#rg").val();
			var email = $("#email").val();
			var telefones = [];
			$(".telefones").each(function(index){
				var telefone = $(this).val();
				telefones.push(telefone);
			});

			//Endereço
			var logradouro = $("#logradouro").val();
			var cidade = $("#cidade").val();
			var bairro = $("#bairro").val();
			var estado = $("#estado").val();
			var uf = $("#uf").val();
			var numero = $("#numero").val();
			var complemento = $("#complemento").val();
			var cep = $("#cep").val();

			//Conta bancária
			var contaBancaria = $("#conta").val();

			var idLocatario = $("#formulario").attr('data-id');
			var tipo = "locatario";
			var acao;
			if(idLocatario == ''){
				acao = "cadastrar";
			} else {
				acao = "alterar";
			}

			$.post("/actControl.php", {"nome":nome, "cpf":cpf, "rg":rg, "email":email, "telefones":telefones, "logradouro":logradouro, "cidade":cidade, "bairro":bairro, "estado":estado, "uf":uf, "numero":numero, "complemento":complemento, "cep":cep, "conta":contaBancaria, "idLocatario":idLocatario, "tipo": tipo, "acao":acao}, function(resposta){

				alert(resposta.mensagem);
				if(resposta.success == true){
					location.reload();
				}
			}, 'json');

		});

		$("#cep").blur(function(){
			var cep = $(this).val();
			if(cep != ""){
				pesquisaCEP(cep);
			}
		});
	});

	
</script>
