    
    function pesquisaCEP(cep){
		//Nova variável "cep" somente com dígitos.
        var cep = cep.replace(/\D/g, '');
        //Cria um elemento javascript.
        var script = document.createElement('script');
        //Sincroniza com o callback.
        script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=retornaEndereco';
        //Insere script no documento e carrega o conteúdo.
        document.body.appendChild(script);
	}

	function retornaEndereco(conteudo) {
        if(conteudo.erro != true){
            //Atualiza os campos com os valores.
            $('#logradouro').val(conteudo.logradouro);
            $('#bairro').val(conteudo.bairro);
            $('#cidade').val(conteudo.localidade);
            $('#uf').val(conteudo.uf);
            var estado = buscaEstado(conteudo.uf);
            if(estado != 0){
            	$('#estado').val(estado);
            } else {
            	alert("Estado não encontrado");
            }
        } else {
            //CEP não Encontrado.
            limpaEndereco();
            alert("CEP não encontrado!");
        }
    }

    function limpaEndereco(){
    	$('#logradouro').val("");
        $('#bairro').val("");
        $('#cidade').val("");
        $('#estado').val("");
        $('#uf').val("");
    }

    function buscaEstado(uf){
    	switch(uf){
    		case 'AC': return 'Acre';
    		case 'AL': return 'Alagoas';
    		case 'AP': return 'Amapá';
    		case 'AM': return 'Amazonas';
    		case 'BA': return 'Bahia';
    		case 'CE': return 'Ceará';
    		case 'DF': return 'Distrito Federal';
    		case 'ES': return 'Espírito Santo';
    		case 'GO': return 'Goiás';
    		case 'MA': return 'Maranhão';
    		case 'MT': return 'Mato Grosso';
    		case 'MS': return 'Mato Grosso do Sul';
    		case 'MG': return 'Minas Gerais';
    		case 'PA': return 'Pará';
    		case 'PB': return 'Paraíba';
    		case 'PR': return 'Paraná';
    		case 'PE': return 'Pernambuco';
    		case 'PI': return 'Piauí';
    		case 'RJ': return 'Rio de Janeiro';
    		case 'RN': return 'Rio Grande do Norte';
    		case 'RS': return 'Rio Grande do Sul';
    		case 'RO': return 'Rondônia';
    		case 'RR': return 'Roraima';
    		case 'SC': return 'Santa Catarina';
    		case 'SP': return 'São Paulo';
    		case 'SE': return 'Sergipe';
    		case 'TO': return 'Tocantins';
    		default: return 0;
    	}
    }