<?php

class EnderecoDao extends Dao{
	
	private $bd = null;

	function __construct(){
		$this->bd = parent::getBancoDados();
	}

	public function cadastrar($endereco){

		$comando = 'insert into endereco(id, logradouro, numero, complemento, cep, idBairro) values (:id, :logradouro, :numero, :complemento, :cep, :idBairro)';
		$parametros = $this->parametros($endereco);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			return true;
		}
		return false;
	}

	public function alterar($endereco){
		$comando = "update endereco set logradouro = :logradouro, numero = :numero, cep = :cep, idBairro = :idBairro, complemento = :complemento where id = :id";
		$parametros = $this->parametros($endereco);

		$alterar = $this->bd->executar($comando, $parametros);

		if($alterar){
			return true;
		}
		return false;
	}

	public function parametros($endereco){
		$parametros = array(
			"id" => $endereco->getId(),
			"logradouro" => $endereco->getLogradouro(),
			"numero" => $endereco->getNumero(),
			"complemento" => $endereco->getComplemento(),
			"cep" => Util::soNumeros($endereco->getCep()),
			"idBairro" => $endereco->getBairro()->getId()
		);

		return $parametros;
	}

	public function existe($endereco){
		/*$comando = "select id from endereco where logradouro = :logradouro and numero = :numero and complemento = :complemento and cep = :cep";
		$parametros = array(
			"logradouro" => $endereco->getLogradouro(),
			"numero" => $endereco->getNumero(),
			"complemento" => $endereco->getComplemento(),
			"cep" => $endereco->getCep()
		);

		$consulta = $this->bd->consultar($comando, $parametros);

		if(count($consulta) > 0){
			return true;
		}
		return false;*/
	}

	public function transformarEmObjeto($arrayLinha){
		$endereco = new Endereco();
		$endereco->setId($arrayLinha['id']);
		$endereco->setLogradouro($arrayLinha['logradouro']);
		$endereco->setNumero($arrayLinha['numero']);
		$endereco->setComplemento($arrayLinha['complemento']);
		$endereco->setCep($arrayLinha['cep']);

		$estado = new Estado();
		$estado->setEstado($arrayLinha['estado']);
		$estado->setUf($arrayLinha['uf']);

		$cidade = new Cidade();
		$cidade->setCidade($arrayLinha['cidade']);
		$cidade->setEstado($estado);

		$bairro = new Bairro();
		$bairro->setBairro($arrayLinha['bairro']);
		$bairro->setCidade($cidade);

		$endereco->setBairro($bairro);

		return $endereco;
	}

	public function obterTodos(){
		$comando = "select endereco.id, endereco.logradouro, endereco.numero, endereco.complemento, endereco.cep, bairro.nome as bairro, cidade.nome as cidade, estado.nome as estado, estado.uf as uf from endereco
				JOIN bairro ON(bairro.id = endereco.idBairro)
				JOIN cidade ON(cidade.id = bairro.idCidade)
				JOIN estado ON(estado.id = cidade.idEstado) where endereco.id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select endereco.id, endereco.logradouro, endereco.numero, endereco.complemento, endereco.cep, bairro.nome as bairro, cidade.nome as cidade, estado.nome as estado, estado.uf as uf from endereco
				JOIN bairro ON(bairro.id = endereco.idBairro)
				JOIN cidade ON(cidade.id = bairro.idCidade)
				JOIN estado ON(estado.id = cidade.idEstado) where endereco.id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($endereco){
		$comando = "delete from endereco where id = :id";
		$parametros = array(
			"id" => $endereco->getId()
		);

		$excluir = $this->bd->executar($comando, $parametros);

		if($excluir){
			return true;
		}
		return false;	
	}

}
?>