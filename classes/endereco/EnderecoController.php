<?php

class EnderecoController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new EnderecoService();
	}

	public function criar(){
		$endereco = new Endereco();

		$camposSimples = array('logradouro', 'numero', 'complemento', 'cep');
		$this->inserirSimples($endereco, $_POST, $camposSimples);

		return $endereco;
	}

	public function cadastrar(){

		$endereco = $this->criar();

		$id = $this->gerarId('endereco');
		$endereco->setId($id);
		
		$bairroController = new BairroController();
		$resultArray = $bairroController->cadastrar();
		$bairro = $resultArray['dados'];
		$endereco->setBairro($bairro);

		$valido = $this->service->validar($endereco);

		if($valido['success']){
			$inserir = $this->service->cadastrar($endereco);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Endereço salvo com sucesso!',
					"dados" => $endereco
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível salvar o endereço!',
					"dados" => $endereco
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($endereco){
		$bairroController = new BairroController();
		$resultArray = $bairroController->cadastrar();
		$bairro = $resultArray['dados'];
		$endereco->setBairro($bairro);

		$valido = $this->service->validar($endereco);
		
		if($valido['success']){
			return $this->service->alterar($endereco);
		} else {
			return $valido;
		}
	}

	public function excluir(){
		return $this->service->excluir($idEndereco);
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}
}
?>