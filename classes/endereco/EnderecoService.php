<?php

class EnderecoService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new EnderecoDao();
	}

	public function cadastrar($endereco){
		return $this->dao->cadastrar($endereco);
	}

	public function validar($endereco){

		$erro = array();

		if($endereco->getCep() == ""){
			$erro['cep'] = "Insira o CEP";
		}

		if($endereco->getLogradouro() == ""){
			$erro['logradouro'] = "Insira o logradouro";
		}

		if($endereco->getNumero() == ""){
			$erro['numero'] = "Insira um número para o endereço";
		}

		if($endereco->getBairro() == null){
			$erro['bairro'] = "Insira o bairro";
		}

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => $endereco
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($endereco){
		return $this->dao->existe($endereco);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($endereco){
		return $this->dao->alterar($endereco);
	}

	public function excluir($endereco){
		return $this->dao->excluir($endereco);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}


}
?>