<?php

class Endereco{

	private $id = ""; //colocar no diagrama
	private $logradouro = "";
	private $numero = "";
	private $complemento = "";
	private $bairro = null;
	private $cep = "";

	function __construct(){
	
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setLogradouro($logradouro){
		$this->logradouro = $logradouro;
	}

	public function getLogradouro(){
		return $this->logradouro;
	}

	public function setNumero($numero){
		$this->numero = $numero;
	}

	public function getNumero(){
		return $this->numero;
	}

	public function setComplemento($complemento){
		$this->complemento = $complemento;
	}

	public function getComplemento(){
		return $this->complemento;
	}

	public function setBairro($objBairro){
		$this->bairro = $objBairro;
	}

	public function getBairro(){
		return $this->bairro;
	}

	public function setCep($cep){
		$this->cep = $cep;
	}

	public function getCep(){
		return $this->cep;
	}

	public function getEnderecoCompleto(){
		$bairro = $this->bairro->getBairro();
		$cidade = $this->bairro->getCidade()->getCidade();
		$uf = $this->bairro->getCidade()->getEstado()->getUf();

		$enderecoCompleto = $this->logradouro.", ".$this->numero." - ".$bairro.", ".$cidade."-".$uf;

		return $enderecoCompleto;
	}

}
?>