<?php

class BairroController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new BairroService();
	}

	public function criar(){
		$bairro = new Bairro();

		$camposSimples = array('bairro');
		$this->inserirSimples($bairro, $_POST, $camposSimples);

		return $bairro;
	}

	public function cadastrar(){

		$bairro = $this->criar();

		$id = $this->gerarId('bairro');
		$bairro->setId($id);

		$cidadeController = new CidadeController();
		$resultArray = $cidadeController->cadastrar();
		$cidade = $resultArray['dados'];
		$bairro->setCidade($cidade);

		$valido = $this->service->validar($bairro);

		if($valido['success']){
			$inserir = $this->service->cadastrar($bairro);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Bairro salvo com sucesso!',
					"dados" => $bairro
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível salvar o Bairro!',
					"dados" => $bairro
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($bairro){
		$valido = $this->service->validar($bairro);
		
		if($valido['success']){
			return $this->service->alterar($bairro);
		} else {
			return $valido;
		}
	}

	public function excluir(){
		return $this->service->excluir($idBairro);
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}
}
?>