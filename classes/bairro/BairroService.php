<?php

class BairroService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new BairroDao();
	}

	public function cadastrar($bairro){
		return $this->dao->cadastrar($bairro);
	}

	public function validar($bairro){

		$erro = array();

		if($bairro->getBairro() == ""){
			$erro['bairro'] = "Insira o nome do bairro";
		}

		if($bairro->getCidade() == null){
			$erro['cidade'] = "Insira a cidade";
		}

		$existe = $this->existe($bairro);
		if($existe > 0){
			$bairro->setId($existe);
		}

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => $bairro
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($bairro){
		return $this->dao->existe($bairro);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($bairro){
		return $this->dao->alterar($bairro);
	}

	public function excluir($bairro){
		return $this->dao->excluir($bairro);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}


}
?>