<?php

class BairroDao extends Dao{
	
	private $bd = null;

	function __construct(){
		$this->bd = parent::getBancoDados();
	}

	public function cadastrar($bairro){

		$comando = 'insert into bairro(id, nome, idCidade) values (:id, :nome, :idCidade)';
		$parametros = $this->parametros($bairro);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			return true;
		}
		return false;
	}

	public function alterar($bairro){
		$comando = "update bairro set nome = :nome, idCidade = :idCidade where id = :id";
		$parametros = $this->parametros($bairro);

		$alterar = $this->bd->executar($comando, $parametros);

		if($alterar){
			return true;
		}
		return false;
	}

	public function parametros($bairro){
		$parametros = array(
			"id" => $bairro->getId(),
			"nome" => $bairro->getBairro(),
			"idCidade" => $bairro->getCidade()->getId()
		);

		return $parametros;
	}

	public function existe($bairro){
		$comando = "select bairro.id from bairro JOIN cidade ON(cidade.id = bairro.idCidade)
			where bairro.nome = :bairro and cidade.nome = :cidade";
		$parametros = array(
			"bairro" => $bairro->getBairro(),
			"cidade" => $bairro->getCidade()->getCidade()
		);

		$consulta = $this->bd->consultar($comando, $parametros);

		if(count($consulta) > 0){
			return $consulta[0]['id'];
		}
		return 0;
	}

	public function transformarEmObjeto($arrayLinha){
		$bairro = new Bairro();
		$bairro->setId($arrayLinha['id']);
		$bairro->setBairro($arrayLinha['bairro']);
		
		$estado = new Estado();
		$estado->setEstado($arrayLinha['estado']);
		$estado->setUf($arrayLinha['uf']);

		$cidade = new Cidade();
		$cidade->setCidade($arrayLinha['cidade']);
		$cidade->setEstado($estado);

		$bairro->setCidade($cidade);

		return $bairro;
	}

	public function obterTodos(){
		$comando = "select bairro.id, bairro.nome as bairro, cidade.nome as cidade, estado.nome as estado, estado.uf as uf from bairro
				JOIN cidade ON(cidade.id = bairro.idCidade)
				JOIN estado ON(estado.id = cidade.idEstado) where bairro.id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select bairro.id, bairro.nome as bairro, cidade.nome as cidade, estado.nome as estado, estado.uf as uf from bairro
				JOIN cidade ON(cidade.id = bairro.idCidade)
				JOIN estado ON(estado.id = cidade.idEstado) where bairro.id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($bairro){
		$comando = "delete from bairro where id = :id";
		$parametros = array(
			"id" => $bairro->getId()
		);

		$excluir = $this->bd->executar($comando, $parametros);

		if($excluir){
			return true;
		}
		return false;	
	}

}
?>