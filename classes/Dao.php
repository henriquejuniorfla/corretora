<?php

abstract class Dao{

	private $bd = null;

	public function getBancoDados(){
		if($this->bd == null)
			$bd = new BancoDados();
		return $bd;
	}

	public function gerarId($nomeTabela){
		return $this->getBancoDados()->gerarId($nomeTabela);
	}

	public abstract function existe($obj);
	public abstract function obterTodos();
	public abstract function obterComId($id);
	public abstract function transformarEmObjeto($array);
	public abstract function cadastrar($obj);
	public abstract function alterar($obj);
	public abstract function excluir($obj);
}
?>