<?php
	
abstract class Tipo implements Enum{

	const TODOS = 0;
	const CASA = 1;
	const APARTAMENTO = 2;

	public static function toString($codigo){
		$areas = array(
			Tipo::TODOS => "Todos",
			Tipo::CASA => "Casa",
			Tipo::APARTAMENTO => "Apartamento"
		);
		return $areas[$codigo];
	}

}

?>