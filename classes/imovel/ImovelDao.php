<?php

class ImovelDao extends Dao{

	private $bd;

	function __construct(){
		$this->bd = $this->getBancoDados();
	}

	public function cadastrar($imovel){

		$comando = 'insert into imovel(id, tamanho, area, tipo, valorAluguel, valorCondominio, valorIptu, idEndereco, idLocatario) values (:id, :tamanho, :area, :tipo, :valorAluguel, :valorCondominio, :valorIptu, :idEndereco, :idLocatario)';
		$parametros = $this->parametros($imovel);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			return true;
		}
		return false;
	}

	public function alterar($imovel){
		$comando = "update imovel set tamanho = :tamanho, area = :area, tipo = :tipo, valorCondominio = :valorCondominio, valorAluguel = :valorAluguel, valorIptu = :valorIptu, idEndereco = :idEndereco, idLocatario = :idLocatario where id = :id";
		$parametros = $this->parametros($imovel);

		try{
			$this->bd->executar($comando, $parametros);
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	public function parametros($imovel){
		$parametros = array(
			"id" => $imovel->getId(),
			"tamanho" => $imovel->getTamanho(),
			"area" => $imovel->getArea(),
			"tipo" => $imovel->getTipoDoImovel(),
			"valorAluguel" => $imovel->getAluguel(),
			"valorCondominio" => $imovel->getCondominio(),
			"valorIptu" => $imovel->getIptu(),
			"idEndereco" => $imovel->getEndereco()->getId(),
			"idLocatario" => $imovel->getLocatario()->getId()
		);

		return $parametros;
	}

	public function existe($imovel){
		// não é necessário
	}

	public function transformarEmObjeto($arrayLinha){
		$imovel = new Imovel();
		$imovel->setId($arrayLinha['id']);
		$imovel->setTamanho($arrayLinha['tamanho']);
		$imovel->setArea($arrayLinha['area']);
		$imovel->setTipoDoImovel($arrayLinha['tipo']);
		$imovel->setAluguel($arrayLinha['valorAluguel']);
		$imovel->setCondominio($arrayLinha['valorCondominio']);
		$imovel->setIptu($arrayLinha['valorIptu']);

		//Locatário
		$locatarioController = new LocatarioController();
		$locatario = $locatarioController->obterComId($arrayLinha['idLocatario']);
		$imovel->setLocatario($locatario);

		//Endereço
		$enderecoController = new EnderecoController();
		$endereco = $enderecoController->obterComId($arrayLinha['idEndereco']);
		$imovel->setEndereco($endereco);

		return $imovel;
	}

	public function obterTodos(){
		$comando = "select id, tamanho, area, tipo, valorAluguel, valorCondominio, valorIptu, idEndereco, idLocatario from imovel where id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select id, tamanho, area, tipo, valorAluguel, valorCondominio, valorIptu, idEndereco, idLocatario from imovel where id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function obterComRestricao($restricoes){
		$comando = "select id, tamanho, area, tipo, valorAluguel, valorCondominio, valorIptu, idEndereco, idLocatario from imovel where id > 0 ";

		if($restricoes['tamanho']){
			$comando .= "and tamanho = :tamanho ";
			$parametros['tamanho'] = $restricoes['tamanho'];
		}

		if($restricoes['area']){
			$comando .= "and area = :area ";
			$parametros['area'] = $restricoes['area'];
		}

		if($restricoes['tipo']){
			$comando .= "and tipo = :tipo ";
			$parametros['tipo'] = $restricoes['tipo'];
		}

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;

	}

	public function excluir($idImovel){
		$comando = "delete from imovel where id = :id";
		$parametros = array(
			"id" => $idLocatario
		);

		$excluir = $this->bd->executar($comando, $parametros);
		if($excluir){
			return true;
		}
		
		return false;	
	}

}
?>