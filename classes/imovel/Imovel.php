<?php

class Imovel{

	private $id = ""; //colocar no diagrama
	private $area = "";
	private $tipo = "";
	private $tamanho = "";
	private $endereco = null;
	private $proprietario = null;
	private $valorCondominio = 0;
	private $valorIptu = 0;
	private $valorAluguel = 0;

	function __construct(){

	}

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setArea($area){
		$this->area = $area;
	}

	public function getArea(){
		return $this->area;
	}

	public function setTipoDoImovel($tipo){
		$this->tipo = $tipo;
	}

	public function getTipoDoImovel(){
		return $this->tipo;
	}

	public function setTamanho($tamanho){
		$this->tamanho = $tamanho;
	}

	public function getTamanho(){
		return $this->tamanho;
	}

	public function setEndereco($objEndereco){
		$this->endereco = $objEndereco;
	}

	public function getEndereco(){
		return $this->endereco;
	}

	public function setLocatario($objLocatario){
		$this->locatario = $objLocatario;
	}

	public function getLocatario(){
		return $this->locatario;
	}

	public function setCondominio($valorCondominio = 0){
		$this->valorCondominio = $valorCondominio;
	}

	public function getCondominio($formatado = false){
		if($formatado){
			return number_format($this->valorCondominio, 2,",",".");
		}
		return $this->valorCondominio;
	}

	public function setIptu($valorIptu = 0){
		$this->valorIptu = $valorIptu;
	}

	public function getIptu($formatado = false){
		if($formatado){
			return number_format($this->valorIptu, 2,",",".");
		}
		return $this->valorIptu;
	}

	public function setAluguel($valorAluguel){
		$this->valorAluguel = $valorAluguel;
	}

	public function getAluguel($formatado = false){
		if($formatado){
			return number_format($this->valorAluguel, 2,",",".");
		}
		return $this->valorAluguel;
	}


}
?>