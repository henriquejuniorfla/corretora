<?php

class ImovelController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new ImovelService();
	}

	public function criar(){
		$imovel = new Imovel();

		$camposSimples = array('tamanho', 'area', 'tipoDoImovel', 'aluguel', 'condominio', 'iptu');
		$this->inserirSimples($imovel, $_POST, $camposSimples);

		return $imovel;
	}

	public function cadastrar(){

		$imovel = $this->criar();

		$id = $this->gerarId('imovel');
		$imovel->setId($id);

		$locatario = null;
		if(isset($_POST['idLocatario']) && is_numeric($_POST['idLocatario'])){
			$locatarioController = new LocatarioController();
			$locatario = $locatarioController->obterComId($_POST['idLocatario']);
		}
		$imovel->setLocatario($locatario);

		if(isset($_POST['estado']) && isset($_POST['uf']) && isset($_POST['cidade']) && isset($_POST['bairro']) && isset($_POST['logradouro']) && isset($_POST['numero']) && isset($_POST['complemento'])){
			//Endereço
			$enderecoController = new EnderecoController();
			$resultArray = $enderecoController->cadastrar();
			$endereco = $resultArray['dados'];
			$imovel->setEndereco($endereco);
		}

		$valido = $this->service->validar($imovel);

		if($valido['success']){
			$inserir = $this->service->cadastrar($imovel);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Imóvel salvo com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível salvar o imóvel!',
					"dados" => $imovel
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($imovel = null){
		if($imovel == null && isset($_POST['idImovel']) && $_POST['idImovel'] != ""){
			$imovel = $this->criar();
			$imovel->setId($_POST['idImovel']);

			$imovelAntigo = $this->obterComId($imovel->getId());
			$idEndereco = $imovelAntigo->getEndereco()->getId();

			$enderecoController = new EnderecoController();
			$endereco = $enderecoController->criar();
			$endereco->setId($idEndereco);
			$enderecoController->alterar($endereco);

			$imovel->setEndereco($endereco);
		}
		
		$valido = $this->service->validar($imovel);
		
		if($valido['success']){
			$alterar = $this->service->alterar($imovel);
			if($alterar){
				$result = array(
					"success" => true,
					"mensagem" => 'Dados do imóvel alterado com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível alterar os dados do imóvel!',
					"dados" => null
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function excluir(){
		if(isset($_POST['idImovel']) && is_numeric($_POST['idImovel'])){
			$excluir =  $this->service->excluir($_POST['idImovel']);
			if($excluir){
				$result = array(
					"success" => true,
					"mensagem" => 'Imóvel excluído com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível excluir o imóvel!',
					"dados" => null
				);
			}
		} else {
			$result = null;
		}
		return $result;
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}

	public function obterComRestricao($restricoes){
		return $this->service->obterComRestricao($restricoes);
	}

}
?>