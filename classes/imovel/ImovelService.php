<?php

class ImovelService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new ImovelDao();
	}

	public function cadastrar($imovel){
		return $this->dao->cadastrar($imovel);
	}

	public function validar($imovel){

		$erro = array();

		//Locatário selecionado
		if($imovel->getLocatario() == null){
			$erro['locatario'] = "Selecione o proprietário do imóvel";
		}
		//Tamanho
		if($imovel->getTamanho() == null || $imovel->getTamanho() == ""){
			$erro['tamanho'] = "Escolha o tamanho do imóvel";
		}
		//Área
		if($imovel->getArea() == null || $imovel->getArea() == ""){
			$erro['area'] = "Escolha a área do imóvel";
		}
		//Tipo do imóvel
		if($imovel->getTipoDoImovel() == null || $imovel->getTipoDoImovel() == ""){
			$erro['tipoDoImovel'] = "Escolha o tipo do imóvel";
		}

		//Endereço
		if($imovel->getEndereco() == null || $imovel->getEndereco()->getLogradouro() == "" || $imovel->getEndereco()->getNumero() == "" || $imovel->getEndereco()->getCep() == ""){
			$erro['endereco'] = "Insira o endereço completo!";
		}

		//Aluguel
		if($imovel->getAluguel() == null || $imovel->getAluguel() == "" || !is_numeric($imovel->getAluguel())){
			$erro['aluguel'] = "Insira o valor do aluguel";
		}
		//Condomínio
		if($imovel->getCondominio() == null || $imovel->getCondominio() == "" || !is_numeric($imovel->getCondominio())){
			$erro['condominio'] = "Insira o valor do condomínio";
		}
		//IPTU
		if($imovel->getIptu() == null || $imovel->getIptu() == "" || !is_numeric($imovel->getIptu())){
			$erro['iptu'] = "Insira o valor do IPTU";
		}

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => null
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($imovel){
		return $this->dao->existe($imovel);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($imovel){
		return $this->dao->alterar($imovel);
	}

	public function excluir($idImovel){
		return $this->dao->excluir($idImovel);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}

	public function obterComRestricao($restricoes){
		return $this->dao->obterComRestricao($restricoes);
	}

}
?>