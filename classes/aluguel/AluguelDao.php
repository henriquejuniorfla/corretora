<?php

class AluguelDao extends Dao{

	private $bd;

	function __construct(){
		$this->bd = $this->getBancoDados();
	}

	public function cadastrar($aluguel){

		$comando = 'insert into aluguel(id, idLocador, idImovel, dataInicio, dataFim, jurosPercentual, multaPercentual, vencimentoAluguel) values (:id, :idLocador, :idImovel, :dataInicio, :dataFim, :jurosPercentual, :multaPercentual, :vencimentoAluguel)';
		$parametros = $this->parametros($aluguel);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			$this->registrarMesesPagamento($aluguel);
			return true;
		}
		return false;
	}

	public function registrarMesesPagamento($aluguel){
		$qtdMeses = 12;
		$prazoVencimento = 1; //meses;
		$dataInicio = date('Y-m-d', strtotime($aluguel->getDataInicio()));

		$comando = "insert pagamento (idAluguel, mesRef, valor) values (:idAluguel, :mesRef, :valor)";

		for($cont=1; $cont<=$qtdMeses; $cont++){
			$dataVencimento = date('Y-m-d', strtotime("+ $prazoVencimento month $dataInicio"));
			
			$parametros = array(
				"idAluguel" => $aluguel->getId(),
				"mesRef" => $dataVencimento,
				"valor" => $aluguel->getImovel()->getAluguel()
			);

			$this->bd->executar($comando, $parametros);
			$prazoVencimento++;
		}
	}

	public function alterar($aluguel){
		$comando = "update aluguel set idLocador = :idLocador, idImovel = :idImovel, dataInicio = :dataInicio, dataFim = :dataFim, jurosPercentual = :jurosPercentual, multaPercentual = :multaPercentual, vencimentoAluguel = :vencimentoAluguel  where id = :id";
		$parametros = $this->parametros($aluguel);

		try{
			$this->bd->executar($comando, $parametros);
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	public function parametros($aluguel){
		$parametros = array(
			"id" => $aluguel->getId(),
			"idLocador" => $aluguel->getLocador()->getId(),
			"idImovel" => $aluguel->getImovel()->getId(),
			"dataInicio" => $aluguel->getDataInicio(),
			"dataFim" => $aluguel->getDataFim(),
			"jurosPercentual" => $aluguel->getJurosPercentual(),
			"multaPercentual" => $aluguel->getMultaPercentual(),
			"vencimentoAluguel" => $aluguel->getVencimentoAluguel()
		);

		return $parametros;
	}

	public function existe($aluguel){
		$comando = "select id from aluguel where idImovel = :idImovel";
		$parametros = array(
			"idImovel" => $aluguel->getImovel()->getId()
		);

		$consulta = $this->bd->consultar($comando, $parametros);

		if(count($consulta) > 0){
			return true;
		}
		return false;
	}

	public function transformarEmObjeto($arrayLinha){
		$aluguel = new Aluguel();
		$aluguel->setId($arrayLinha['id']);
		$aluguel->setDataInicio($arrayLinha['dataInicio']);
		$aluguel->setDataFim($arrayLinha['dataFim']);
		$aluguel->setJurosPercentual($arrayLinha['jurosPercentual']);
		$aluguel->setMultaPercentual($arrayLinha['multaPercentual']);
		$aluguel->setVencimentoAluguel($arrayLinha['vencimentoAluguel']);

		$locadorController = new LocadorController();
		$locador = $locadorController->obterComId($arrayLinha['idLocador']);
		$aluguel->setLocador($locador);

		$imovelController = new ImovelController();
		$imovel = $imovelController->obterComId($arrayLinha['idImovel']);
		$aluguel->setImovel($imovel);

		return $aluguel;
	}

	public function obterTodos(){
		$comando = "select * from aluguel where id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select * from aluguel where id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function obterComIdImovel($idImovel){
		$comando = "select * from aluguel where idImovel = :idImovel";
		$parametros = array(
			"idImovel" => $idImovel
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($idAluguel){
		$comando = "delete from aluguel where id = :id";
		$parametros = array(
			"id" => $idAluguel
		);

		$excluir = $this->bd->executar($comando, $parametros);
		if($excluir){
			return true;
		}
		
		return false;	
	}

	public function obterPagamentosAtrasados($idAluguel){
		$comando = "select * from pagamento where idAluguel = :idAluguel and mesRef < :dataAtual and pago = 0";
		$parametros = array(
			"idAluguel" => $idAluguel,
			"dataAtual" => date("Y-m-d")
		);

		return $this->bd->consultar($comando, $parametros);
	}

	public function renovarContrato($aluguel){
		$comando = "update aluguel set dataInicio = :dataInicio and dataFim = :dataFim and vencimentoAluguel = :vencimentoAluguel where id = :id";
		$parametros = array(
			"dataInicio" => $aluguel->getDataInicio(),
			"dataFim" => $aluguel->getDataFim(),
			"vencimentoAluguel" => $aluguel->getVencimentoAluguel(),
			"id" => $aluguel->getId()
		);

		$renovou = $this->bd->executar($comando, $parametros);
		if($renovou){
			$this->registrarMesesPagamento($aluguel);
			return true;
		}
		return false;
	}

}
?>