<?php

class AluguelService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new AluguelDao();
	}

	public function cadastrar($aluguel){
		return $this->dao->cadastrar($aluguel);
	}

	public function validar($aluguel){

		$erro = array();

		if($this->existe($aluguel)){
			$erro['existe'] = "O imóvel já foi alugado para outro cliente";
		}

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => null
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($aluguel){
		return $this->dao->existe($aluguel);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($aluguel){
		return $this->dao->alterar($aluguel);
	}

	public function excluir($idAluguel){
		return $this->dao->excluir($idAluguel);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}

	public function obterComIdImovel($idImovel){
		return $this->dao->obterComIdImovel($idImovel);
	}

	public function obterPagamentosAtrasados($idAluguel){
		return $this->dao->obterPagamentosAtrasados($idAluguel);
	}

	public function renovarContrato($aluguel){
		return $this->dao->renovarContrato($aluguel);
	}

}
?>