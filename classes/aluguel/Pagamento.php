<?php

class Pagamento{

	private $id = ""; //colocar no diagrama
	private $aluguel = null;
	private $valor = null;
	private $mesReferencia = "";

	function __construct($objAluguel){
		$this->aluguel = $objAluguel;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setAluguel($objAluguel = null){
		$this->aluguel = $objAluguel;
	}

	public function getAluguel(){
		return $this->aluguel;
	}

	public function setValor($valor){
		$this->valor = $valor;
	}

	public function getValor(){
		return $this->valor;
	}

	public function setMesReferencia($mesReferencia = null){
		$this->mesReferencia = $mesReferencia;
	}

	public function getMesReferencia(){
		return $this->mesReferencia;
	}
}
?>