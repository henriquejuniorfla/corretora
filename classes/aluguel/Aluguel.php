<?php

class Aluguel{

	private $id = ""; //colocar no diagrama
	private $multaPercentual = 0;
	private $vencimentoAluguel = "";
	private $dataInicio = "";
	private $dataFim = "";
	private $jurosPercentual = 0; 
	private $valorTotalAluguel = 0; 
	private $imovel = null;
	private $locador = null; 

	function __construct(){
		
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setMultaPercentual($multaPercentual = null){
		$this->multaPercentual = $multaPercentual;
	}

	public function getMultaPercentual(){
		return $this->multaPercentual;
	}

	public function setVencimentoAluguel($vencimentoAluguel = null){
		$this->vencimentoAluguel = $vencimentoAluguel;
	}

	public function getVencimentoAluguel(){
		return $this->vencimentoAluguel;
	}

	public function setDataInicio($dataInicio = null){
		$this->dataInicio = $dataInicio;
	}

	public function getDataInicio(){
		return $this->dataInicio;
	}

	public function setDataFim($dataFim = null){
		$this->dataFim = $dataFim;
	}

	public function getDataFim(){
		return $this->dataFim;
	}

	public function setImovel($objImovel = null){
		$this->imovel = $objImovel;
	}

	public function getImovel(){
		return $this->imovel;
	}

	public function setJurosPercentual($jurosPercentual = null){
		$this->jurosPercentual = $jurosPercentual;
	}

	public function getJurosPercentual(){
		return $this->jurosPercentual;
	}

	public function getValorMulta(){
		$valorMulta = ($this->multaPercentual * $this->imovel->getValorAluguel()) /100;
		return $valorMulta;
	}

	public function getValorJuros(){
		$valorJuros = ($this->jurosPercentual * $this->imovel->getValorAluguel()) /100;
		return $valorJuros;
	}

	public function getValorTotalAluguel(){
		$valorTotal = $this->imovel->getValorAluguel() + ($this->imovel->getValorIptu()/12);

		if($this->$imovel->getTipo() == Tipo::APARTAMENTO){
			$valorTotal += $this->imovel->getValorCondominio(); 
		}

		if($this->estaAtrasado()){
			$valorTotal += ($this->getValorMulta() + $this->getValorJuros());
		}

		return $valorTotal;
	}

	public function setLocador($objLocador = null){
		$this->locador = $objLocador;
	}

	public function getLocador(){
		return $this->locador;
	}

}
?>