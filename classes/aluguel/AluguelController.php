<?php

class AluguelController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new AluguelService();
	}

	public function criar(){
		$aluguel = new Aluguel();

		$prazoAluguel = 1; //anos;
		$dataInicio = date('Y-m-d', strtotime($_POST['dataInicio']));
		$dataFim = date('Y-m-d', strtotime("+ $prazoAluguel year $dataInicio"));

		$_POST['dataFim'] = $dataFim;

		$camposSimples = array('dataInicio', 'dataFim'); 
		$this->inserirSimples($aluguel, $_POST, $camposSimples);

		return $aluguel;
	}

	public function cadastrar(){

		$aluguel = $this->criar();

		$id = $this->gerarId('aluguel');
		$aluguel->setId($id);

		if(isset($_POST['dataInicio'])){
			$aluguel->setVencimentoAluguel(date('0000-00-d', strtotime($_POST['dataInicio'])));
		}

		if(isset($_POST['idImovel']) && isset($_POST['idLocador']) && is_numeric($_POST['idImovel']) && is_numeric($_POST['idLocador'])){
			$imovelController = new ImovelController();
			$imovel = $imovelController->obterComId($_POST['idImovel']);
			$aluguel->setImovel($imovel);

			$locadorController = new LocadorController();
			$locador = $locadorController->obterComId($_POST['idLocador']);
			$aluguel->setLocador($locador);

			$valido = $this->service->validar($aluguel);

		} else {
			$valido['success'] = false;
		}

		if($valido['success']){
			$inserir = $this->service->cadastrar($aluguel);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Aluguel realizado com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível alugar o imóvel!',
					"dados" => $aluguel
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($aluguel = null){
		if($aluguel == null && isset($_POST['idAluguel']) && $_POST['idAluguel'] != ""){
			$aluguel = $this->criar();
			$aluguel->setId($_POST['idAluguel']);
		}
		
		$valido = $this->service->validar($aluguel);
		
		if($valido['success']){
			$alterar = $this->service->alterar($aluguel);
			if($alterar){
				$result = array(
					"success" => true,
					"mensagem" => 'Dados do aluguel alterado com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível alterar os dados do aluguel!',
					"dados" => null
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function excluir(){
		if(isset($_POST['idAluguel']) && is_numeric($_POST['idAluguel'])){
			$excluir =  $this->service->excluir($_POST['idAluguel']);
			if($excluir){
				$result = array(
					"success" => true,
					"mensagem" => 'Aluguel excluído com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível excluir o aluguel!',
					"dados" => null
				);
			}
		} else {
			$result = null;
		}
		return $result;
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}

	public function obterComIdImovel($idImovel){
		return $this->service->obterComIdImovel($idImovel);
	}

	public function obterPagamentosAtrasados($idAluguel){
		return $this->service->obterPagamentosAtrasados($idAluguel);
	}

	public function renovarContrato(){

		$renovou = false;

		if(isset($_POST['aluguel']) && is_numeric($_POST['aluguel'])){
			$aluguelController = new AluguelController();
			$aluguel = $aluguelController->obterComId($_POST['aluguel']);
			$dataInicio = date("Y-m-d");
			$dataFim = date("Y-m-d", strtotime("+ 1 year $dataInicio"));

			$aluguel->setDataInicio($dataInicio);
			$aluguel->setDataFim($dataFim);
			$aluguel->setVencimentoAluguel(date('Y-m-d', strtotime($dataInicio)));

			$renovou = $this->service->renovarContrato($aluguel);
		}
		
		if($renovou){
			$result = array(
				"success" => true,
				"mensagem" => 'Aluguel renovado com sucesso!',
				"dados" => null
			);	
		} else {
			$result = array(
				"success" => false,
				"mensagem" => 'Não foi possível renovar o aluguel!',
				"dados" => null
			);
		}
		return $result;
	}

}
?>