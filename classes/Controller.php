<?php

abstract class Controller{
	public abstract function criar();
	public abstract function cadastrar();
	//public abstract function gerenciar();
	public abstract function alterar($obj);
	public abstract function excluir();
	public abstract function obterTodos();
	public abstract function obterComId($id);

	public function inserirSimples($obj, $metodoHTTP, $arrayCampos){
		foreach($arrayCampos as $campo){
			if(isset($metodoHTTP[$campo])){
				$metodoFuncao = 'set'.trim(ucfirst($campo));
				$obj->$metodoFuncao($metodoHTTP[$campo]);
			}
		}

		return $obj;
	}

}
?>