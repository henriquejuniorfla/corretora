<?php

class ConexaoSingleton{

	private static $conexao = null;

	private function __construct(){} //private para a classe não ser instanciada

	public static function getConexao(){
		try{
			$bdh = 'mysql:dbname=corretora;host=localhost';
			$usuario = 'root';
			$senha = '';

			if(self::$conexao == null){
				self::$conexao = new PDO($bdh, $usuario, $senha);
			}

			return self::$conexao;
			
		} catch(PDOException $e){
			echo 'Falha na conexão: '.$e->getMessage();
		}
	}


}
?>