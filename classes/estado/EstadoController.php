<?php

class EstadoController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new EstadoService();
	}

	public function criar(){
		$estado = new Estado();

		$camposSimples = array('estado', 'uf');
		$this->inserirSimples($estado, $_POST, $camposSimples);

		return $estado;
	}

	public function cadastrar(){

		$estado = $this->criar();

		$id = $this->gerarId('estado');
		$estado->setId($id);
		
		$valido = $this->service->validar($estado);

		if($valido['success']){
			$inserir = $this->service->cadastrar($estado);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Estado salvo com sucesso!',
					"dados" => $estado
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível salvar o estado!',
					"dados" => $estado
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($estado){
		$valido = $this->service->validar($estado);
		
		if($valido['success']){
			return $this->service->alterar($estado);
		} else {
			return $valido;
		}
	}

	public function excluir(){
		return $this->service->excluir($idEstado);
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}
}
?>