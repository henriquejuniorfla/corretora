<?php

class EstadoDao extends Dao{
	
	private $bd = null;

	function __construct(){
		$this->bd = parent::getBancoDados();
	}

	public function cadastrar($estado){

		$comando = 'insert into estado(id, nome, uf) values (:id, :nome, :uf)';
		$parametros = $this->parametros($estado);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			return true;
		}
		return false;
	}

	public function alterar($estado){
		$comando = "update estado set nome = :nome, uf = :uf where id = :id";
		$parametros = $this->parametros($estado);

		$alterar = $this->bd->executar($comando, $parametros);

		if($alterar){
			return true;
		}
		return false;
	}

	public function parametros($estado){
		$parametros = array(
			"id" => $estado->getId(),
			"nome" => $estado->getEstado(),
			"uf" => strtoupper($estado->getUf())
		);

		return $parametros;
	}

	public function existe($estado){
		$comando = "select id from estado where nome = :nome and uf = :uf";
		$parametros = array(
			"nome" => $estado->getEstado(),
			"uf" => $estado->getUf()
		);

		$consulta = $this->bd->consultar($comando, $parametros);
		
		if(count($consulta) > 0){
			return $consulta[0]['id'];
		}
		return 0;
	}

	public function transformarEmObjeto($arrayLinha){
		$estado = new Estado();
		$estado->setId($arrayLinha['id']);
		$estado->setEstado($arrayLinha['estado']);
		$estado->setUf($arrayLinha['uf']);

		return $estado;
	}

	public function obterTodos(){
		$comando = "select estado.id, estado.nome as estado, estado.uf as uf from estado.id where id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select estado.id, estado.nome as estado, estado.uf as uf from estado where estado.id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($estado){
		$comando = "delete from estado where id = :id";
		$parametros = array(
			"id" => $estado->getId()
		);

		$excluir = $this->bd->executar($comando, $parametros);

		if($excluir){
			return true;
		}
		return false;	
	}

}
?>