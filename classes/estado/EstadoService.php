<?php

class EstadoService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new EstadoDao();
	}

	public function cadastrar($estado){
		return $this->dao->cadastrar($estado);
	}

	public function validar($estado){

		$erro = array();

		if($estado->getEstado() == ""){
			$erro['estado'] = "Insira o nome do estado";
		}

		if($estado->getUf() == ""){
			$erro['uf'] = "Insira a UF do estado";
		}

		$existe = $this->existe($estado);
		if($existe > 0){
			$estado->setId($existe);
		}

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => $estado
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($estado){
		return $this->dao->existe($estado);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($estado){
		return $this->dao->alterar($estado);
	}

	public function excluir($estado){
		return $this->dao->excluir($estado);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}


}
?>