<?php
	
abstract class Cliente{

	private $id = ""; //colocar no diagrama
	private $nome = "";
	private $cpf = "";
	private $telefone = null;
	private $rg = "";
	private $email = "";

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setNome($nome = null){
		$this->nome = $nome;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setCpf($cpf = null){
		$this->cpf = $cpf;
	}

	public function getCpf(){
		return $this->cpf;
	}

	public function setTelefone($objTelefone = null){
		$this->telefone = $objTelefone;
	}

	public function getTelefone(){
		return $this->telefone;
	}

	public function setRg($rg){
		$this->rg = $rg;
	}

	public function getRg(){
		return $this->rg;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getEmail(){
		return $this->email;
	}

}
?>