<?php
	
abstract class Area implements Enum{

	const TODAS = 0;
	const CENTRO = 1;
	const ZONA_NORTE = 2;
	const ZONA_SUL = 3;
	const ZONA_OESTE = 4;

	public static function toString($codigo){
		$areas = array(
			Area::TODAS => "Todas",
			Area::CENTRO => "Centro",
			Area::ZONA_NORTE => "Zona Norte",
			Area::ZONA_SUL => "Zona Sul",
			Area::ZONA_OESTE => "Zona Oeste"
		);
		return $areas[$codigo];
	}

}

?>