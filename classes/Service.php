<?php

abstract class Service{

	public abstract function validar($obj);
	public abstract function existe($obj);
	public abstract function obterTodos();
	public abstract function obterComId($id);
	public abstract function cadastrar($obj);
	public abstract function alterar($obj);
	public abstract function excluir($obj);
	
}
?>