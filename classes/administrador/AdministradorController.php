<?php

class AdministradorController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new AdministradorService();
	}

	public function criar(){
		$administrador = new Administrador();
		$id = $this->gerarId('administrador');
		$administrador->setId($id);

		$camposSimples = array('nome', 'cpf', 'funcao', 'email', 'usuario');
		$this->inserirSimples($administrador, $_POST, $camposSimples);

		if(isset($_POST['telefones']) || count($_POST['telefones'] > 0)){
			$telefone = new Telefone();
			$telefones = $_POST['telefones'];
			foreach($telefones as $numero){
				$telefone->setNumero($numero);
			}
			$administrador->setTelefone($telefone);
		}

		if(isset($_POST['senha']) && $_POST['senha'] != ""){
			$senha = sha1($_POST['senha']);
			$administrador->setSenha($senha);
		}

		return $administrador;
	}

	public function cadastrar(){

		$administrador = $this->criar();
		$valido = $this->service->validar($administrador);

		if($valido['success']){
			$inserir = $this->service->cadastrar($administrador);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Administrador salvo com sucesso!',
					"dados" => $administrador->getNome()
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível salvar o administrador!',
					"dados" => null
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($administrador){
		$valido = $this->service->validar($administrador);
		
		if($valido['success']){
			$administrador->setSenha(sha1($administrador->getSenha()));
			return $this->service->alterar($administrador);
		} else {
			return $valido;
		}
	}

	public function excluir(){
		return $this->service->excluir($idAdministrador);
	}

	public function logar($identificacao, $senha){
		if(!isset($_SESSION['id'])){
			$administrador = $this->service->obterComLoginESenha($identificacao, $senha);
			if(isset($administrador)){
				session_start();
				$_SESSION['id'] = $administrador->getId();
			}
		}
	}

	public function deslogar(){
		if(isset($_SESSION['id'])){
			unset($_SESSION['id']);
			session_destroy();
		}
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}

}
?>