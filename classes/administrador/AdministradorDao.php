<?php

class AdministradorDao extends Dao{
	
	private $bd = null;

	function __construct(){
		$this->bd = parent::getBancoDados();
	}

	public function cadastrar($administrador){
		//por enquanto, não estamos salvando o telefone do administrador

		$comando = 'insert into administrador(id, nome, cpf, funcao, email, usuario, senha) values (:id, :nome, :cpf, :funcao, :email, :usuario, :senha)';
		$parametros = $this->parametros($administrador);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			return true;
		}
		return false;
	}

	public function alterar($administrador){
		$comando = "update administrador set nome = :nome, cpf = :cpf, email = :email, funcao = :funcao, usuario = :usuario, senha = :senha where id = :id";
		$parametros = $this->parametros($administrador);

		$alterar = $this->bd->executar($comando, $parametros);

		if($alterar){
			return true;
		}
		return false;
	}

	public function parametros($administrador){
		$parametros = array(
			"id" => $administrador->getId(),
			"nome" => $administrador->getNome(),
			"cpf" => $administrador->getCpf(),
			"funcao" => $administrador->getFuncao(),
			"email" => $administrador->getEmail(),
			"usuario" => $administrador->getUsuario(),
			"senha" => $administrador->getSenha()
		);

		return $parametros;
	}

	public function existe($administrador){
		$comando = "select id from administrador where usuario = :usuario or email = :email or cpf = :cpf";
		$parametros = array(
			"usuario" => $administrador->getUsuario(),
			"email" => $administrador->getEmail(),
			"cpf" => $administrador->getCpf()
		);

		$consulta = $this->bd->consultar($comando, $parametros);

		if(count($consulta) > 0){
			return true;
		}
		return false;
	}

	public function transformarEmObjeto($arrayLinha){
		$administrador = new Administrador();
		$administrador->setId($arrayLinha['id']);
		$administrador->setNome($arrayLinha['nome']);
		$administrador->setCpf($arrayLinha['cpf']);
		$administrador->setFuncao($arrayLinha['funcao']);
		$administrador->setEmail($arrayLinha['email']);
		$administrador->setUsuario($arrayLinha['usuario']);

		return $administrador;
	}

	public function obterTodos(){
		$comando = "select id, nome, cpf, funcao, email, usuario from administrador where id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select id, nome, cpf, funcao, email, usuario from administrador where id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function obterComLoginESenha($identificacao, $senha){
		$comando = "select id, nome, cpf, funcao, email, usuario from administrador where (email = :identificacao or usuario = :identificacao) and senha = :senha";
		$parametros = array(
			"identificacao" => trim($identificacao),
			"senha" => trim(sha1($senha))
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($administrador){
		$comando = "delete from administrador where id = :id";
		$parametros = array(
			"id" => $administrador->getId()
		);

		$excluir = $this->bd->executar($comando, $parametros);

		if($excluir){
			return true;
		}
		return false;	
	}

}
?>