<?php

class Administrador extends Cliente{

	private $funcao = "";
	private $email = "";
	private $usuario = "";
	private $senha = "";

	function __construct(){}

	public function setFuncao($funcao){
		$this->funcao = $funcao;
	}

	public function getFuncao(){
		return $this->funcao;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}

	public function getUsuario(){
		return $this->usuario;
	}

	public function setSenha($senha){
		$this->senha = $senha;
	}

	public function getSenha(){
		return $this->senha;
	}
}
?>