<?php

class AdministradorService extends Service{

	private $dao = null;
	const TAM_MIN_USUARIO = 5;
	
	function __construct(){
		$this->dao = new AdministradorDao();
	}

	public function cadastrar($administrador){
		return $this->dao->cadastrar($administrador);
	}

	public function validar($administrador){

		$erro = array();

		//Nome
		if($administrador->getNome() == null || $administrador->getNome() == ""){
			$erro['nome'] = "Preencha o nome do administrador";
		}
		//CPF
		if($administrador->getCpf() == null || $administrador->getCpf() == ""){
			$erro['cpf'] = "Digite um CPF";
		} else {
			if(!Util::validaCPF($administrador->getCpf())){
				$erro['cpf'] = "Digite um CPF válido!";
			}
		}
		//Função
		if($administrador->getFuncao() == null || $administrador->getFuncao() == ""){
			$erro['funcao'] = "Escolha uma função";
		}
		//Email
		if($administrador->getEmail() == null || $administrador->getEmail() == ""){
			$erro['email'] = "Digite um email";
		} else {
			if(!Util::validaEmail($administrador->getEmail())){
				$erro['email'] = "Digite um email válido!";
			}
		}
		//Usuário
		if($administrador->getUsuario() == null || $administrador->getUsuario() == ""){
			$erro['usuario'] = "Digite um nome de usuário";
		} else {
			if(strlen($administrador->getUsuario()) < AdministradorService::TAM_MIN_USUARIO){
				$erro['usuario'] = "Usuário deve ter no mínimo ".AdministradorService::TAM_MIN_USUARIO." caracteres";
			} else {
				if(!Util::validaNomeUsuario($administrador->getUsuario())){
					$erro['usuario'] = "Caracteres inválidos no nome do usuário";
				}
			}
		}
		//Senha
		if($administrador->getSenha() == null || $administrador->getSenha() == ""){
			$erro['senha'] = "Digite uma senha";
		}

		if($this->existe($administrador)){
			$erro['usuario'] = "Usuário já existente! CPF, email ou nome de usuário iguais.";
		}

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => null
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($administrador){
		return $this->dao->existe($administrador);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($administrador){
		return $this->dao->alterar($administrador);
	}

	public function excluir($administrador){
		return $this->dao->excluir($administrador);
	}

	public function obterComLoginESenha($identificacao, $senha){
		return $this->dao->obterComLoginESenha($identificacao, $senha);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}


}
?>