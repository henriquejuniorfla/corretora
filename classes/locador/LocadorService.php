<?php

class LocadorService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new LocadorDao();
	}

	public function cadastrar($locador){
		return $this->dao->cadastrar($locador);
	}

	public function validar($locador){

		$erro = array();

		//Nome
		if($locador->getNome() == null || $locador->getNome() == ""){
			$erro['nome'] = "Preencha o nome do locador";
		}
		//CPF
		if($locador->getCpf() == null || $locador->getCpf() == ""){
			$erro['cpf'] = "Digite um CPF";
		} else {
			if(!Util::validaCPF($locador->getCpf())){
				$erro['cpf'] = "Digite um CPF válido!";
			}
		}
		if($this->existe($locador) && isset($_POST['idLocador']) && $_POST['idLocador'] == ""){
			$erro['cpf'] = "Locador já existente! CPF já cadastrado!";
		}
		//Email
		if($locador->getEmail() == null || $locador->getEmail() == ""){
			$erro['email'] = "Digite um email";
		} else {
			if(!Util::validaEmail($locador->getEmail())){
				$erro['email'] = "Digite um email válido!";
			}
		}		
		//Telefone
		if($locador->getTelefone() == null){
			$erro['telefone'] = "Insira um número de telefone.";
		}	

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => null
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($locador){
		return $this->dao->existe($locador);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($locador){
		return $this->dao->alterar($locador);
	}

	public function excluir($idLocador){
		return $this->dao->excluir($idLocador);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}

	public function obterTelefones($idLocador){
		return $this->dao->obterTelefones($idLocador);
	}


}
?>