<?php

class LocadorDao extends Dao{

	private $bd;

	function __construct(){
		$this->bd = $this->getBancoDados();
	}

	public function cadastrar($locador){

		$comando = 'insert into locador(id, nome, cpf, rg, email) values (:id, :nome, :cpf, :rg, :email)';
		$parametros = $this->parametros($locador);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			//Inserindo telefones
			$this->cadastrarTelefones($locador);
			return true;
		}
		return false;
	}

	public function alterar($locador){
		$comando = "update locador set nome = :nome, cpf = :cpf, rg = :rg, email = :email where id = :id";
		$parametros = $this->parametros($locador);

		try{
			$this->bd->executar($comando, $parametros);
			$excluir = $this->excluirTelefones($locador->getId());
			if($excluir){
				$this->cadastrarTelefones($locador);
			}
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	public function parametros($locador){
		$parametros = array(
			"id" => $locador->getId(),
			"nome" => $locador->getNome(),
			"cpf" => Util::soNumeros($locador->getCpf()),
			"rg" => $locador->getRg(),
			"email" => $locador->getEmail()
		);

		return $parametros;
	}

	public function existe($locador){
		$comando = "select id from locador where cpf = :cpf";
		$parametros = array(
			"cpf" => Util::soNumeros($locador->getCpf())
		);

		$consulta = $this->bd->consultar($comando, $parametros);

		if(count($consulta) > 0){
			return true;
		}
		return false;
	}

	public function transformarEmObjeto($arrayLinha){
		$locador = new Locador();
		$locador->setId($arrayLinha['id']);
		$locador->setNome($arrayLinha['nome']);
		$locador->setCpf($arrayLinha['cpf']);
		$locador->setRg($arrayLinha['rg']);
		$locador->setEmail($arrayLinha['email']);

		return $locador;
	}

	public function obterTodos(){
		$comando = "select id, nome, cpf, rg, email from locador where id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select id, nome, cpf, rg, email from locador where id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($idLocador){
		$comando = "delete from locador where id = :id";
		$parametros = array(
			"id" => $idLocador
		);

		$excluirTelefones = $this->excluirTelefones($idLocador);
		if($excluirTelefones){
			$excluir = $this->bd->executar($comando, $parametros); //exclui locador
			if($excluir){
				return true;
			}
		}
		
		return false;	
	}

	public function transformarEmObjetoTelefone($arrayLinha){
		$telefone = new Telefone();
		$telefone->setNumero($arrayLinha['numero']);

		return $telefone;
	}

	public function obterTelefones($idLocador){
		$comando = "select telefone_locador.numero from locador join telefone_locador ON(locador.id = telefone_locador.idLocador) where locador.id = :idLocador";
		$parametros = array(
			"idLocador" => $idLocador
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjetoTelefone'));
		return $arrayObjs;
	}

	public function cadastrarTelefones($locador){
		$telefone = $locador->getTelefone();
		$telefones = $telefone->getNumeros();

		foreach($telefones as $numTelefone){
			$comando = 'insert into telefone_locador(numero, idLocador) values (:numero, :idLocador)';
			$parametrosTel = array(
				"numero" => $numTelefone,
				"idLocador" => $locador->getId()
			);
			$this->bd->executar($comando, $parametrosTel);
		}
	}

	public function excluirTelefones($idLocador){
		$comando = "delete from telefone_locador where idLocador = :idLocador";
		$parametros = array(
			"idLocador" => $idLocador
		);

		$excluir = $this->bd->executar($comando, $parametros);

		if($excluir){
			return true;
		}
		return false;
	}

}
?>