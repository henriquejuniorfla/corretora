<?php

class LocadorController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new LocadorService();
	}

	public function criar(){
		$locador = new Locador();

		$camposSimples = array('nome', 'cpf', 'rg', 'email');
		$this->inserirSimples($locador, $_POST, $camposSimples);

		if(isset($_POST['telefones']) || count($_POST['telefones'] > 0)){
			$telefone = new Telefone();
			$telefones = $_POST['telefones'];
			foreach($telefones as $numero){
				if($numero != "")
					$telefone->setNumero($numero);
			}
			$locador->setTelefone($telefone);
		}

		return $locador;
	}

	public function cadastrar(){

		$locador = $this->criar();

		$id = $this->gerarId('locador');
		$locador->setId($id);

		$valido = $this->service->validar($locador);

		if($valido['success']){
			$inserir = $this->service->cadastrar($locador);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Locador salvo com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível salvar o locador!',
					"dados" => $locador
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($locador = null){
		if($locador == null && isset($_POST['idLocador']) && $_POST['idLocador'] != ""){
			$locador = $this->criar();
			$locador->setId($_POST['idLocador']);
		}
		
		$valido = $this->service->validar($locador);
		
		if($valido['success']){
			$alterar = $this->service->alterar($locador);
			if($alterar){
				$result = array(
					"success" => true,
					"mensagem" => 'Dados do locador alterado com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível alterar os dados do locador!',
					"dados" => null
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function excluir(){
		if(isset($_POST['idLocador']) && is_numeric($_POST['idLocador'])){
			$excluir =  $this->service->excluir($_POST['idLocador']);
			if($excluir){
				$result = array(
					"success" => true,
					"mensagem" => 'Locador excluído com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível excluir o locador!',
					"dados" => null
				);
			}
		} else {
			$result = null;
		}
		return $result;
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}

	public function obterTelefones($idLocador){
		return $this->service->obterTelefones($idLocador);
	}
}
?>