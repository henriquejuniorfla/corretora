<?php

class Visitacao{

	private $id = ""; //colocar no diagrama
	private $imovel = null; //atualizar no diagrama
	private $locador = null; //atualizar no diagrama
	private $dataEntregaChave = "";
	private $dataDevolucaoChave = "";
	private $disponivel = 1;

	function __construct(){

	}

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setImovel($objImovel = null){
		$this->imovel = $objImovel;
	}

	public function getImovel(){
		return $this->imovel;
	}

	public function setLocador($objLocador = null){
		$this->locador = $objLocador;
	}

	public function getLocador(){
		return $this->locador;
	}

	public function setDataEntregaChave($dataEntregaChave = null){
		$this->dataEntregaChave = $dataEntregaChave;
	}

	public function getDataEntregaChave(){
		return $this->dataEntregaChave;
	}

	public function setDataDevolucaoChave($dataDevolucaoChave = null){
		$this->dataDevolucaoChave = $dataDevolucaoChave;
	}

	public function getDataDevolucaoChave(){
		return $this->dataDevolucaoChave;
	}

	public function setDisponivel($disponivel){
		$this->disponivel = $disponivel;
	}

	public function getDisponivel(){
		return $this->disponivel;
	}


}
?>