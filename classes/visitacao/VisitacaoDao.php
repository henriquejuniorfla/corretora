<?php

class VisitacaoDao extends Dao{

	private $bd;

	function __construct(){
		$this->bd = $this->getBancoDados();
	}

	public function cadastrar($visitacao){

		$comando = 'insert into visitacao(id, idLocador, idImovel, dataEntregaChave, dataDevolucaoChave, disponivel) values (:id, :idLocador, :idImovel, :dataEntregaChave, :dataDevolucaoChave, :disponivel)';
		$parametros = $this->parametros($visitacao);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			return true;
		}
		return false;
	}

	public function alterar($visitacao){
		$comando = "update visitacao set idLocador = :idLocador, idImovel = :idImovel, dataEntregaChave = :dataEntregaChave, dataDevolucaoChave = :dataDevolucaoChave, disponivel = :disponivel where id = :id";
		$parametros = $this->parametros($visitacao);

		try{
			$this->bd->executar($comando, $parametros);
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	public function parametros($visitacao){
		$parametros = array(
			"id" => $visitacao->getId(),
			"idLocador" => $visitacao->getLocador()->getId(),
			"idImovel" => $visitacao->getImovel()->getId(),
			"dataEntregaChave" => $visitacao->getDataEntregaChave(),
			"dataDevolucaoChave" => $visitacao->getDataDevolucaoChave(),
			"disponivel" => $visitacao->getDisponivel()
		);

		return $parametros;
	}

	public function existe($visitacao){
		$comando = "select id from visitacao where idImovel = :idImovel and disponivel = 0";
		$parametros = array(
			"idImovel" => $visitacao->getImovel()->getId()
		);

		$consulta = $this->bd->consultar($comando, $parametros);

		if(count($consulta) > 0){
			return true;
		}
		return false;
	}

	public function transformarEmObjeto($arrayLinha){
		$visitacao = new Visitacao();
		$visitacao->setId($arrayLinha['id']);
		$visitacao->setDataEntregaChave($arrayLinha['dataEntregaChave']);
		$visitacao->setDataDevolucaoChave($arrayLinha['dataDevolucaoChave']);
		$visitacao->setDisponivel($arrayLinha['disponivel']);

		$locadorController = new LocadorController();
		$locador = $locadorController->obterComId($arrayLinha['idLocador']);
		$visitacao->setLocador($locador);

		$imovelController = new ImovelController();
		$imovel = $imovelController->obterComId($arrayLinha['idImovel']);
		$visitacao->setImovel($imovel);

		return $visitacao;
	}

	public function obterTodos(){
		$comando = "select * from visitacao where id > :id and disponivel = 0";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select * from visitacao where id = :id and disponivel = 0";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($idVisitacao){
		$comando = "delete from visitacao where id = :id";
		$parametros = array(
			"id" => $idVisitacao
		);

		$excluir = $this->bd->executar($comando, $parametros);
		if($excluir){
			return true;
		}
		
		return false;	
	}

	public function marcarComoDisponivel($idVisitacao){
		$comando = "update visitacao set disponivel = 1 where id = :id";
		$parametros = array(
			"id" => $idVisitacao
		);

		$this->bd->executar($comando, $parametros);
	}

}
?>