<?php

class VisitacaoController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new VisitacaoService();
	}

	public function criar(){
		$visitacao = new Visitacao();

		$camposSimples = array('dataEntregaChave', 'dataDevolucaoChave'); 
		$this->inserirSimples($visitacao, $_POST, $camposSimples);

		return $visitacao;
	}

	public function cadastrar(){

		$visitacao = $this->criar();

		$id = $this->gerarId('visitacao');
		$visitacao->setId($id);

		$visitacao->setDisponivel(0); //Quando cadastra ele fica indisponível

		if(isset($_POST['idImovel']) && isset($_POST['idLocador']) && is_numeric($_POST['idImovel']) && is_numeric($_POST['idLocador'])){
			$imovelController = new ImovelController();
			$imovel = $imovelController->obterComId($_POST['idImovel']);
			$visitacao->setImovel($imovel);

			$locadorController = new LocadorController();
			$locador = $locadorController->obterComId($_POST['idLocador']);
			$visitacao->setLocador($locador);

			$valido = $this->service->validar($visitacao);

		} else {
			$valido['success'] = false;
		}

		if($valido['success']){
			$inserir = $this->service->cadastrar($visitacao);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Visitação agendada com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível agendar a visitação!',
					"dados" => $visitacao
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($visitacao = null){
		if($visitacao == null && isset($_POST['idVisitacao']) && $_POST['idVisitacao'] != ""){
			$visitacao = $this->criar();
			$visitacao->setId($_POST['idVisitacao']);
		}
		
		$valido = $this->service->validar($visitacao);
		
		if($valido['success']){
			$alterar = $this->service->alterar($visitacao);
			if($alterar){
				$result = array(
					"success" => true,
					"mensagem" => 'Dados da visitação alterado com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível alterar os dados da visitação!',
					"dados" => null
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function excluir(){
		if(isset($_POST['idVisitacao']) && is_numeric($_POST['idVisitacao'])){
			$excluir =  $this->service->excluir($_POST['idVisitacao']);
			if($excluir){
				$result = array(
					"success" => true,
					"mensagem" => 'Visitação excluída com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível excluir a visitação!',
					"dados" => null
				);
			}
		} else {
			$result = null;
		}
		return $result;
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}

	public function marcarComoDisponivel($idVisitacao = null){
		if($idVisitacao == null && isset($_POST['idVisitacao']) && is_numeric($_POST['idVisitacao'])){
			$idVisitacao = $_POST['idVisitacao'];
		}
		$this->service->marcarComoDisponivel($idVisitacao);
	}

}
?>