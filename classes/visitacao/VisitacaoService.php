<?php

class VisitacaoService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new VisitacaoDao();
	}

	public function cadastrar($visitacao){
		return $this->dao->cadastrar($visitacao);
	}

	public function validar($visitacao){

		$erro = array();
		$dataAtual = date('Y-m-d');

		if($visitacao->getDataEntregaChave() == "" || $visitacao->getDataDevolucaoChave() == ""){
			$erro['data'] = "Defina a data de entrega e devolução da chave";
		} else {
			if(strtotime($visitacao->getDataEntregaChave()) > strtotime($visitacao->getDataDevolucaoChave())){
				$erro['data'] = "A data de entrega das chaves deve ser menor ou igual a data de devolução das chaves";
			}

			if(strtotime($visitacao->getDataDevolucaoChave()) < $dataAtual){
				$erro['data'] = "A data de devolução da chave deve ser maior ou igual do que a data atual";
			}
		}

		if($this->existe($visitacao)){
			$erro['existe'] = "O imóvel já foi agendado para outro cliente";
		}

		//validação se o imóvel já está alugado
		$aluguelController = new AluguelController();
		$aluguel = $aluguelController->obterComIdImovel($visitacao->getImovel()->getId());
		if($aluguel != null){
			$erro['existe'] = "O imóvel já foi alugado para outro cliente";
		}

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => null
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($visitacao){
		return $this->dao->existe($visitacao);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($visitacao){
		return $this->dao->alterar($visitacao);
	}

	public function excluir($idVisitacao){
		return $this->dao->excluir($idVisitacao);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}

	public function marcarComoDisponivel($idVisitacao){
		$this->dao->marcarComoDisponivel($idVisitacao);
	}


}
?>