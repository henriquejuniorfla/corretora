<?php

class CidadeController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new CidadeService();
	}

	public function criar(){
		$cidade = new Cidade();

		$camposSimples = array('cidade');
		$this->inserirSimples($cidade, $_POST, $camposSimples);

		return $cidade;
	}

	public function cadastrar(){

		$cidade = $this->criar();

		$id = $this->gerarId('cidade');
		$cidade->setId($id);

		$estadoController = new EstadoController();
		$resultArray = $estadoController->cadastrar();
		$estado = $resultArray['dados'];
		$cidade->setEstado($estado);

		$valido = $this->service->validar($cidade);

		if($valido['success']){
			$inserir = $this->service->cadastrar($cidade);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Cidade salva com sucesso!',
					"dados" => $cidade
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível salvar o cidade!',
					"dados" => $cidade
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($cidade){
		$valido = $this->service->validar($cidade);
		
		if($valido['success']){
			return $this->service->alterar($cidade);
		} else {
			return $valido;
		}
	}

	public function excluir(){
		return $this->service->excluir($idCidade);
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}
}
?>