<?php

class CidadeService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new CidadeDao();
	}

	public function cadastrar($cidade){
		return $this->dao->cadastrar($cidade);
	}

	public function validar($cidade){

		$erro = array();

		if($cidade->getCidade() == ""){
			$erro['cidade'] = "Insira o nome da cidade";
		}

		if($cidade->getEstado() == null){
			$erro['estado'] = "Insira o estado";
		}

		$existe = $this->existe($cidade);
		if($existe > 0){
			$cidade->setId($existe);
		}

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => $cidade
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($cidade){
		return $this->dao->existe($cidade);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($cidade){
		return $this->dao->alterar($cidade);
	}

	public function excluir($cidade){
		return $this->dao->excluir($cidade);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}


}
?>