<?php

class CidadeDao extends Dao{
	
	private $bd = null;

	function __construct(){
		$this->bd = parent::getBancoDados();
	}

	public function cadastrar($cidade){

		$comando = 'insert into cidade(id, nome, idEstado) values (:id, :nome, :idEstado)';
		$parametros = $this->parametros($cidade);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			return true;
		}
		return false;
	}

	public function alterar($cidade){
		$comando = "update cidade set nome = :nome, idEstado = :idEstado where id = :id";
		$parametros = $this->parametros($cidade);

		$alterar = $this->bd->executar($comando, $parametros);

		if($alterar){
			return true;
		}
		return false;
	}

	public function parametros($cidade){
		$parametros = array(
			"id" => $cidade->getId(),
			"nome" => $cidade->getCidade(),
			"idEstado" => $cidade->getEstado()->getId()
		);

		return $parametros;
	}

	public function existe($cidade){
		$comando = "select cidade.id from cidade JOIN estado ON(estado.id = cidade.idEstado) where cidade.nome = :cidade and estado.nome = :estado";
		$parametros = array(
			"cidade" => $cidade->getCidade(),
			"estado" => $cidade->getEstado()->getEstado()
		);

		$consulta = $this->bd->consultar($comando, $parametros);
		
		if(count($consulta) > 0){
			return $consulta[0]['id'];
		}
		return false;
	}

	public function transformarEmObjeto($arrayLinha){
		$estado = new Estado();
		$estado->setEstado($arrayLinha['estado']);
		$estado->setUf($arrayLinha['uf']);

		$cidade = new Cidade();
		$cidade->setId($arrayLinha['id']);
		$cidade->setCidade($arrayLinha['cidade']);
		$cidade->setEstado($estado);

		return $cidade;
	}

	public function obterTodos(){
		$comando = "select cidade.id, cidade.nome as cidade, estado.nome as estado, estado.uf as uf from cidade
				JOIN estado ON(estado.id = cidade.idEstado) where cidade.id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select cidade.id, cidade.nome as cidade, estado.nome as estado, estado.uf as uf from cidade
				JOIN estado ON(estado.id = cidade.idEstado) where cidade.id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($cidade){
		$comando = "delete from cidade where id = :id";
		$parametros = array(
			"id" => $cidade->getId()
		);

		$excluir = $this->bd->executar($comando, $parametros);

		if($excluir){
			return true;
		}
		return false;	
	}

}
?>