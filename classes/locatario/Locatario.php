<?php
	
class Locatario extends Cliente{

	private $conta = "";
	private $endereco = null;

	function __construct(){

	}

	public function setConta($conta = null){
		$this->conta = $conta;
	}

	public function getConta(){
		return $this->conta;
	}

	public function setEndereco($ObjEndereco = null){
		$this->endereco = $ObjEndereco;
	}

	public function getEndereco(){
		return $this->endereco;
	}
}
?>