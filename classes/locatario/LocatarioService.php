<?php

class LocatarioService extends Service{

	private $dao = null;
	
	function __construct(){
		$this->dao = new LocatarioDao();
	}

	public function cadastrar($locatario){
		return $this->dao->cadastrar($locatario);
	}

	public function validar($locatario){

		$erro = array();

		//Nome
		if($locatario->getNome() == null || $locatario->getNome() == ""){
			$erro['nome'] = "Preencha o nome do locatário";
		}
		//CPF
		if($locatario->getCpf() == null || $locatario->getCpf() == ""){
			$erro['cpf'] = "Digite um CPF";
		} else {
			if(!Util::validaCPF($locatario->getCpf())){
				$erro['cpf'] = "Digite um CPF válido!";
			}
		}
		if($this->existe($locatario) && isset($_POST['idLocatario']) && $_POST['idLocatario'] == ""){
			$erro['cpf'] = "Locatário já existente! CPF já cadastrado!";
		}
		//Email
		if($locatario->getEmail() == null || $locatario->getEmail() == ""){
			$erro['email'] = "Digite um email";
		} else {
			if(!Util::validaEmail($locatario->getEmail())){
				$erro['email'] = "Digite um email válido!";
			}
		}		
		//Telefone
		if($locatario->getTelefone() == null){
			$erro['telefone'] = "Insira um número de telefone.";
		}
		//Endereço
		if($locatario->getEndereco() == null || $locatario->getEndereco()->getLogradouro() == "" || $locatario->getEndereco()->getNumero() == "" || $locatario->getEndereco()->getCep() == ""){
			$erro['endereco'] = "Insira o endereço completo!";
		}
		//Conta
		if($locatario->getConta() == ""){
			$erro['conta'] = "Insira o número da conta";
		}		

		if(count($erro) > 0){
			$result = array(
				"success" => false,
				"mensagem" => array_shift($erro),
				"dados" => null
			);
		} else {
			$result = array(
				"success" => true,
				"mensagem" => 'Dados validados com sucesso!',
				"dados" => null
			);
		}

		return $result;
	}

	public function existe($locatario){
		return $this->dao->existe($locatario);
	}

	public function obterTodos(){
		return $this->dao->obterTodos();
	}

	public function obterComId($id){
		return $this->dao->obterComId($id);
	}

	public function alterar($locatario){
		return $this->dao->alterar($locatario);
	}

	public function excluir($idLocatario){
		return $this->dao->excluir($idLocatario);
	}

	public function gerarId($nomeTabela){
		return $this->dao->gerarId($nomeTabela);
	}

	public function obterTelefones($idLocatario){
		return $this->dao->obterTelefones($idLocatario);
	}


}
?>