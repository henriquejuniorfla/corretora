<?php

class LocatarioDao extends Dao{

	private $bd;

	function __construct(){
		$this->bd = $this->getBancoDados();
	}

	public function cadastrar($locatario){

		$comando = 'insert into locatario(id, nome, cpf, rg, conta, email, idEndereco) values (:id, :nome, :cpf, :rg, :conta, :email, :idEndereco)';
		$parametros = $this->parametros($locatario);
		$inserir = $this->bd->executar($comando, $parametros);

		if($inserir){
			//Inserindo telefones
			$this->cadastrarTelefones($locatario);
			return true;
		}
		return false;
	}

	public function alterar($locatario){
		$comando = "update locatario set nome = :nome, cpf = :cpf, rg = :rg, email = :email, conta = :conta, idEndereco = :idEndereco where id = :id";
		$parametros = $this->parametros($locatario);

		try{
			$this->bd->executar($comando, $parametros);
			$excluir = $this->excluirTelefones($locatario->getId());
			if($excluir){
				$this->cadastrarTelefones($locatario);
			}
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	public function parametros($locatario){
		$parametros = array(
			"id" => $locatario->getId(),
			"nome" => $locatario->getNome(),
			"cpf" => Util::soNumeros($locatario->getCpf()),
			"rg" => $locatario->getRg(),
			"conta" => $locatario->getConta(),
			"email" => $locatario->getEmail(),
			"idEndereco" => $locatario->getEndereco()->getId()
		);

		return $parametros;
	}

	public function existe($locatario){
		$comando = "select id from locatario where cpf = :cpf";
		$parametros = array(
			"cpf" => Util::soNumeros($locatario->getCpf())
		);

		$consulta = $this->bd->consultar($comando, $parametros);

		if(count($consulta) > 0){
			return true;
		}
		return false;
	}

	public function transformarEmObjeto($arrayLinha){
		$locatario = new Locatario();
		$locatario->setId($arrayLinha['id']);
		$locatario->setNome($arrayLinha['nome']);
		$locatario->setCpf($arrayLinha['cpf']);
		$locatario->setRg($arrayLinha['rg']);
		$locatario->setConta($arrayLinha['conta']);
		$locatario->setEmail($arrayLinha['email']);

		//Endereço
		$enderecoController = new EnderecoController();
		$endereco = $enderecoController->obterComId($arrayLinha['idEndereco']);
		$locatario->setEndereco($endereco);

		return $locatario;
	}

	public function obterTodos(){
		$comando = "select id, nome, cpf, rg, conta, email, idEndereco from locatario where id > :id";
		$parametros = array(
			"id" => 0
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjeto'));
		return $arrayObjs;
	}

	public function obterComId($id){
		$comando = "select id, nome, cpf, rg, conta, email, idEndereco from locatario where id = :id";
		$parametros = array(
			"id" => $id
		);

		$obj = $this->bd->obterObjeto($comando, $parametros, array($this,'transformarEmObjeto'));
		return $obj;
	}

	public function excluir($idLocatario){
		$comando = "delete from locatario where id = :id";
		$parametros = array(
			"id" => $idLocatario
		);

		$excluirTelefones = $this->excluirTelefones($idLocatario);
		if($excluirTelefones){
			$excluir = $this->bd->executar($comando, $parametros); //exclui locatário
			if($excluir){
				return true;
			}
		}
		
		return false;	
	}

	public function transformarEmObjetoTelefone($arrayLinha){
		$telefone = new Telefone();
		$telefone->setNumero($arrayLinha['numero']);

		return $telefone;
	}

	public function obterTelefones($idLocatario){
		$comando = "select telefone_locatario.numero from locatario join telefone_locatario ON(locatario.id = telefone_locatario.idLocatario) where locatario.id = :idLocatario";
		$parametros = array(
			"idLocatario" => $idLocatario
		);

		$arrayObjs = $this->bd->obterObjetos($comando, $parametros, array($this,'transformarEmObjetoTelefone'));
		return $arrayObjs;
	}

	public function cadastrarTelefones($locatario){
		$telefone = $locatario->getTelefone();
		$telefones = $telefone->getNumeros();

		foreach($telefones as $numTelefone){
			$comando = 'insert into telefone_locatario(numero, idLocatario) values (:numero, :idLocatario)';
			$parametrosTel = array(
				"numero" => $numTelefone,
				"idLocatario" => $locatario->getId()
			);
			$this->bd->executar($comando, $parametrosTel);
		}
	}

	public function excluirTelefones($idLocatario){
		$comando = "delete from telefone_locatario where idLocatario = :idLocatario";
		$parametros = array(
			"idLocatario" => $idLocatario
		);

		$excluir = $this->bd->executar($comando, $parametros);

		if($excluir){
			return true;
		}
		return false;
	}

}
?>