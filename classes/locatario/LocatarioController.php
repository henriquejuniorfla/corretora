<?php

class LocatarioController extends Controller{

	private $service = null;

	function __construct(){
		$this->service = new LocatarioService();
	}

	public function criar(){
		$locatario = new Locatario();

		$camposSimples = array('nome', 'cpf', 'rg', 'conta', 'email');
		$this->inserirSimples($locatario, $_POST, $camposSimples);

		if(isset($_POST['telefones']) || count($_POST['telefones'] > 0)){
			$telefone = new Telefone();
			$telefones = $_POST['telefones'];
			foreach($telefones as $numero){
				if($numero != "")
					$telefone->setNumero($numero);
			}
			$locatario->setTelefone($telefone);
		}

		return $locatario;
	}

	public function cadastrar(){

		$locatario = $this->criar();

		$id = $this->gerarId('locatario');
		$locatario->setId($id);

		if(isset($_POST['estado']) && isset($_POST['uf']) && isset($_POST['cidade']) && isset($_POST['bairro']) && isset($_POST['logradouro']) && isset($_POST['numero']) && isset($_POST['complemento'])){
			//Endereço
			$enderecoController = new EnderecoController();
			$resultArray = $enderecoController->cadastrar();
			$endereco = $resultArray['dados'];
			$locatario->setEndereco($endereco);
		}

		$valido = $this->service->validar($locatario);

		if($valido['success']){
			$inserir = $this->service->cadastrar($locatario);
			if($inserir){
				$result = array(
					"success" => true,
					"mensagem" => 'Locatário salvo com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível salvar o locatário!',
					"dados" => $locatario
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function obterTodos(){
		return $this->service->obterTodos();
	}

	public function obterComId($id){
		return $this->service->obterComId($id);
	}

	public function alterar($locatario = null){
		if($locatario == null && isset($_POST['idLocatario']) && $_POST['idLocatario'] != ""){
			$locatario = $this->criar();
			$locatario->setId($_POST['idLocatario']);

			$locatarioAntigo = $this->obterComId($locatario->getId());
			$idEndereco = $locatarioAntigo->getEndereco()->getId();

			$enderecoController = new EnderecoController();
			$endereco = $enderecoController->criar();
			$endereco->setId($idEndereco);
			$enderecoController->alterar($endereco);

			$locatario->setEndereco($endereco);
		}
		
		$valido = $this->service->validar($locatario);
		
		if($valido['success']){
			$alterar = $this->service->alterar($locatario);
			if($alterar){
				$result = array(
					"success" => true,
					"mensagem" => 'Dados do locatário alterado com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível alterar os dados do locatário!',
					"dados" => null
				);
			}
		} else {
			$result = $valido;
		}

		return $result;
	}

	public function excluir(){
		if(isset($_POST['idLocatario']) && is_numeric($_POST['idLocatario'])){
			$excluir =  $this->service->excluir($_POST['idLocatario']);
			if($excluir){
				$result = array(
					"success" => true,
					"mensagem" => 'Locatário excluído com sucesso!',
					"dados" => null
				);	
			} else {
				$result = array(
					"success" => false,
					"mensagem" => 'Não foi possível excluir o locatário!',
					"dados" => null
				);
			}
		} else {
			$result = null;
		}
		return $result;
	}

	public function gerarId($nomeTabela){
		return $this->service->gerarId($nomeTabela);
	}

	public function obterTelefones($idLocatario){
		return $this->service->obterTelefones($idLocatario);
	}
}
?>