<?php

class BancoDados{

	private $bd = null;

	function __construct(){
		$this->bd = ConexaoSingleton::getConexao();
	}

	public function executar($comando, $parametros){
		$prepara = $this->bd->prepare($comando);
		$prepara->execute($parametros);

		$linhasAfetadas = $prepara->rowCount();
		if($linhasAfetadas > 0){
			return true;
		}
		return false;
	}

	public function consultar($comando, $parametros){
		$prepara = $this->bd->prepare($comando);
		$prepara->execute($parametros);
		$query = $prepara->fetchAll(PDO::FETCH_ASSOC);

		return $query; //retorna um array com 1 ou mais linhas da tabela
	}

	public function obterObjeto($comando, $parametros, $callback){
		$arrayLinha = $this->consultar($comando, $parametros);

		if(count($arrayLinha) > 0)
			$obj = call_user_func_array($callback, $arrayLinha);
		else
			$obj = null;

		return $obj;
	}
	
	public function obterObjetos($comando, $parametros, $callback){
		$arrayLinha = $this->consultar($comando, $parametros);

		if(count($arrayLinha) > 0){
			$arrayObjs = array();
			foreach($arrayLinha as $linha){
				$arrayParam = array($linha);
				$obj = call_user_func_array($callback, $arrayParam);
				array_push($arrayObjs, $obj);
			}
			return $arrayObjs;
		}
		
		return null;
	}

	public function gerarId($nomeTabela){
		$comando = "select id from ".$nomeTabela." order by id desc limit 1";
		$prepara = $this->bd->prepare($comando);
		$prepara->execute();
		$query = $prepara->fetch(PDO::FETCH_ASSOC);
		$add = 1;
		if($query == false)
			return $add;
		else
			return (int)$query['id']+$add;
	}

}
?>