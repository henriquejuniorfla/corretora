<?php
	
abstract class Tamanho implements Enum{

	const QUALQUER = 0;
	const UM_QUARTO = 1;
	const DOIS_QUARTO = 2;
	const TRES_QUARTO = 3;
	const QUATRO_OU_MAIS_QUARTOS = 4;
	const CONJUGADO = 5;

	public static function toString($codigo){
		$tamanhos = array(
			Tamanho::QUALQUER => "Qualquer",
			Tamanho::UM_QUARTO => "Um Quarto",
			Tamanho::DOIS_QUARTO => "Dois Quartos",
			Tamanho::TRES_QUARTO => "Três Quartos",
			Tamanho::QUATRO_OU_MAIS_QUARTOS => "Quatro Quartos ou Mais",
			Tamanho::CONJUGADO => "Conjugado"
		);
		return $tamanhos[$codigo];
	}

}

?>