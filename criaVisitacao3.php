<?php
	require_once("util/autoload.php");
	
	$imovelController = new ImovelController();
	$imovel = null;
	if(isset($_GET['id'])){
		$imovel = $imovelController->obterComId($_GET['id']);
	}

	$locador = null;
	if(isset($_GET['locador'])){
		$idLocador = $_GET['locador'];
		$locadorController = new LocadorController();
		$locador = $locadorController->obterComId($idLocador);
	}

	if($imovel == null || $locador == null){
		die("Imóvel ou cliente inválidos");
	}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>HAM Corretora - Criar Visitação</title>
	<link rel="shortcut icon" href="img/logo.ico" type="image/x-icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="css/owl.theme.default.css" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="css/magnific-popup.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />
	<link type="text/css" rel="stylesheet" href="css/styleCadastro.css" />

</head>

<body>
	<!-- Header -->
	<header>

		<!-- Nav -->
		<nav id="nav" class="navbar">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="index.html">
							<img class="logo" src="img/logo.png" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.html#home">Home</a></li>
					<li><a href="index.html#clientes">Clientes</a></li>
					<li><a href="index.html#imoveis">Imóveis</a></li>
					<li><a href="index.html#controle">Controle</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-9">
					<div class="blog">
						<!-- form -->
						<div class="reply-form">
							<h3 class="title">Criar Visitação</h3>

							<input type="hidden" name="locador" id="locador" value="<?php echo $locador->getId()?>">
							<input type="hidden" name="imovel" id="imovel" value="<?php echo $imovel->getId()?>">


							<form id="formulario">

								<h4>Cliente</h4>
								<?php
									if($locador != null){
										echo "<p>".$locador->getNome()."</p>";
									}
								?>

								<h4>Imóvel</h4>

								<div class="imovelinfo">
									<img src="img/casa1.jpg">
									<div class="col.md-2">
										<p>Endereço: <?php echo $imovel->getEndereco()->getEnderecoCompleto();?></p>
										<p>CEP: <?php echo $imovel->getEndereco()->getCep();?></p>
										
										<p>Tamanho: <?php echo Tamanho::toString($imovel->getTamanho());?></p>
										<p>Tipo: <?php echo Tipo::toString($imovel->getTipoDoImovel());?></p>

										<h4>Valor</h4>
										<p>Aluguel: R$ <?php echo $imovel->getAluguel(true)?></p>
										<p>Condomínio: R$ <?php echo $imovel->getCondominio(true)?></p>
										<p>IPTU: R$ <?php echo $imovel->getIptu(true)?></p>

										<h4>Proprietário</h4>
										<p><?php echo $imovel->getLocatario()->getNome()?></p>
									</div>
								</div>

								<div class="date">
									<h4>Entrega da Chave</h4>
									<input type="date" name="entregaChave" id="entregaChave">
								</div>
								<div class="date">
									<h4>Devolução da Chave</h4>
									<input type="date" name="devolucaoChave" id="devolucaoChave">
								</div><br>

								<div class="col-md-12">
									<button type="button" class="main-btn cadastrar">Marcar Visita</button>
								</div>
							</form>
						</div>
						<!-- /form -->
					</div>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->

	<!-- Footer -->
	<footer id="footer" class="sm-padding bg-dark">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<div class="col-md-12">

					<!-- footer logo -->
					<div class="footer-logo">
						<a href="index.html"><img src="img/logo-alt.png" alt="logo"></a>
					</div>
					<!-- /footer logo -->

					<!-- footer copyright -->
					<div class="footer-copyright">
						<p>Copyright © 2017. All Rights Reserved. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
					</div>
					<!-- /footer copyright -->

				</div>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>

<script type="text/javascript">
	
	$(function(){

		$(".cadastrar").click(function(){
			var idLocador = $("#locador").val();
			var idImovel = $("#imovel").val();
			var dataEntregaChave = $("#entregaChave").val();
			var dataDevolucaoChave = $("#devolucaoChave").val();

			var tipo = "visitacao";
			var acao = "cadastrar";

			$.post("/actControl.php", {"idLocador": idLocador, "idImovel": idImovel, "dataEntregaChave": dataEntregaChave, "dataDevolucaoChave": dataDevolucaoChave, "tipo": tipo, "acao": acao}, function(resposta){

				alert(resposta.mensagem);
				if(resposta.success == true){
					location.href="/index.html";
				}
			}, 'json');
		});
	});
</script>